-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Czas generowania: 18 Lut 2021, 19:02
-- Wersja serwera: 8.0.22
-- Wersja PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `symfony_db`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `account`
--

CREATE TABLE `account` (
  `id` int NOT NULL,
  `kind_id` int NOT NULL,
  `manager_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `account`
--

INSERT INTO `account` (`id`, `kind_id`, `manager_id`, `name`, `reference`) VALUES
(1, 1, 1, 'Bank Millennium', 'Mill_360');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `account_history`
--

CREATE TABLE `account_history` (
  `id` int NOT NULL,
  `account_id` int DEFAULT NULL,
  `current_value` double NOT NULL,
  `budget_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `budget`
--

CREATE TABLE `budget` (
  `id` int NOT NULL,
  `manager_id` int NOT NULL,
  `start_date` datetime NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:dateinterval)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `budget`
--

INSERT INTO `budget` (`id`, `manager_id`, `start_date`, `duration`) VALUES
(1, 1, '2021-02-01 00:00:00', 'P1M');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cash_set`
--

CREATE TABLE `cash_set` (
  `id` int NOT NULL,
  `account_id` int NOT NULL,
  `budget_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cash_set_entry`
--

CREATE TABLE `cash_set_entry` (
  `id` int NOT NULL,
  `cash_set_id` int NOT NULL,
  `face_value_id` int NOT NULL,
  `value` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dictionary`
--

CREATE TABLE `dictionary` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `dictionary`
--

INSERT INTO `dictionary` (`id`, `description`, `extra`) VALUES
('MANAGER_ROLES', 'role użytkowników', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dictionary_item`
--

CREATE TABLE `dictionary_item` (
  `id` int NOT NULL,
  `dictionary_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `dictionary_item`
--

INSERT INTO `dictionary_item` (`id`, `dictionary_id`, `item`, `description`, `extra`) VALUES
(1, 'MANAGER_ROLES', 'ROLE_USER', 'domyślna rola użytkownika', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `manager`
--

CREATE TABLE `manager` (
  `id` int NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `manager`
--

INSERT INTO `manager` (`id`, `username`, `password`) VALUES
(1, 'admin', 'haslo'),
(2, 'marek', '$argon2id$v=19$m=65536,t=4,p=1$Umx4SUJzZTNWeFpHbVIycQ$7q/puXukcoiu6wU+13dA9DpAe4hfuYFtP+vwElxA9u0'),
(3, 'Jan', '$argon2id$v=19$m=65536,t=4,p=1$ZXFSUDc5ZkhzaU5rQkhHUA$EKYVgqbTPVtET8cMMGfAHGKI4A2rG2FG0R1XlmNV5Zk'),
(4, 'ks', '$argon2id$v=19$m=65536,t=4,p=1$L1liNjJjTEg0elA2WDhHYQ$16zOSw7mdsuiCZ4/ubFmjYM6NlKAvrAz+Dr4TWryGIg'),
(5, 'Aldona', '$argon2id$v=19$m=65536,t=4,p=1$YUZ5VlhjMThOV3RvaGNheQ$6FW0yh8Yt0QoN782PrKWs46OxnRDnI05f8oVjVjpn00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `manager_role`
--

CREATE TABLE `manager_role` (
  `manager_id` int NOT NULL,
  `role_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `planning`
--

CREATE TABLE `planning` (
  `id` int NOT NULL,
  `kind_id` int NOT NULL,
  `department_id` int NOT NULL,
  `planned` double NOT NULL DEFAULT '0',
  `remained` double NOT NULL DEFAULT '0',
  `used` double NOT NULL DEFAULT '0',
  `prepaid` tinyint(1) NOT NULL DEFAULT '0',
  `budget_id` int NOT NULL,
  `closed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `summary`
--

CREATE TABLE `summary` (
  `id` int NOT NULL,
  `budget_id` int NOT NULL,
  `account_id` int NOT NULL,
  `total_incomes` double NOT NULL,
  `total_outcomes` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `summary`
--

INSERT INTO `summary` (`id`, `budget_id`, `account_id`, `total_incomes`, `total_outcomes`) VALUES
(1, 1, 1, 1539.14, 675);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `transaction`
--

CREATE TABLE `transaction` (
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget_id` int NOT NULL,
  `planning_id` int DEFAULT NULL,
  `place_id` int NOT NULL,
  `account_id` int NOT NULL,
  `trans_date` datetime NOT NULL,
  `account_date` datetime DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quote` decimal(10,2) NOT NULL,
  `after_transaction` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7D3656A430602CA9` (`kind_id`),
  ADD UNIQUE KEY `UNIQ_7D3656A4783E3463` (`manager_id`);

--
-- Indeksy dla tabeli `account_history`
--
ALTER TABLE `account_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EE9164039B6B5FBA` (`account_id`),
  ADD KEY `IDX_EE91640336ABA6B8` (`budget_id`);

--
-- Indeksy dla tabeli `budget`
--
ALTER TABLE `budget`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_73F2F77B783E3463` (`manager_id`);

--
-- Indeksy dla tabeli `cash_set`
--
ALTER TABLE `cash_set`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B8489FB99B6B5FBA` (`account_id`),
  ADD KEY `IDX_B8489FB936ABA6B8` (`budget_id`);

--
-- Indeksy dla tabeli `cash_set_entry`
--
ALTER TABLE `cash_set_entry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5A675CEA22105F60` (`cash_set_id`),
  ADD KEY `IDX_5A675CEADD84B607` (`face_value_id`);

--
-- Indeksy dla tabeli `dictionary`
--
ALTER TABLE `dictionary`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `dictionary_item`
--
ALTER TABLE `dictionary_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_F93C6D55AF5E5B3C1F1B251E` (`dictionary_id`,`item`),
  ADD KEY `IDX_F93C6D55AF5E5B3C` (`dictionary_id`);

--
-- Indeksy dla tabeli `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_FA2425B9F85E0677` (`username`);

--
-- Indeksy dla tabeli `manager_role`
--
ALTER TABLE `manager_role`
  ADD PRIMARY KEY (`manager_id`,`role_id`),
  ADD KEY `IDX_51602813783E3463` (`manager_id`),
  ADD KEY `IDX_51602813D60322AC` (`role_id`);

--
-- Indeksy dla tabeli `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D499BFF630602CA9` (`kind_id`),
  ADD KEY `IDX_D499BFF636ABA6B8` (`budget_id`),
  ADD KEY `IDX_D499BFF6AE80F5DF` (`department_id`);

--
-- Indeksy dla tabeli `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CE28666336ABA6B8` (`budget_id`),
  ADD UNIQUE KEY `UNIQ_CE2866639B6B5FBA` (`account_id`);

--
-- Indeksy dla tabeli `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `IDX_723705D136ABA6B8` (`budget_id`),
  ADD KEY `IDX_723705D13D865311` (`planning_id`),
  ADD KEY `IDX_723705D1DA6A219` (`place_id`),
  ADD KEY `IDX_723705D19B6B5FBA` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `account`
--
ALTER TABLE `account`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `account_history`
--
ALTER TABLE `account_history`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `budget`
--
ALTER TABLE `budget`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `cash_set`
--
ALTER TABLE `cash_set`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `cash_set_entry`
--
ALTER TABLE `cash_set_entry`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `dictionary_item`
--
ALTER TABLE `dictionary_item`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `manager`
--
ALTER TABLE `manager`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `planning`
--
ALTER TABLE `planning`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `FK_7D3656A430602CA9` FOREIGN KEY (`kind_id`) REFERENCES `dictionary_item` (`id`),
  ADD CONSTRAINT `FK_7D3656A4783E3463` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`);

--
-- Ograniczenia dla tabeli `account_history`
--
ALTER TABLE `account_history`
  ADD CONSTRAINT `FK_EE91640336ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  ADD CONSTRAINT `FK_EE9164039B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`);

--
-- Ograniczenia dla tabeli `budget`
--
ALTER TABLE `budget`
  ADD CONSTRAINT `FK_73F2F77B783E3463` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`);

--
-- Ograniczenia dla tabeli `cash_set`
--
ALTER TABLE `cash_set`
  ADD CONSTRAINT `FK_B8489FB936ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  ADD CONSTRAINT `FK_B8489FB99B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`);

--
-- Ograniczenia dla tabeli `cash_set_entry`
--
ALTER TABLE `cash_set_entry`
  ADD CONSTRAINT `FK_5A675CEA22105F60` FOREIGN KEY (`cash_set_id`) REFERENCES `cash_set` (`id`),
  ADD CONSTRAINT `FK_5A675CEADD84B607` FOREIGN KEY (`face_value_id`) REFERENCES `dictionary_item` (`id`);

--
-- Ograniczenia dla tabeli `dictionary_item`
--
ALTER TABLE `dictionary_item`
  ADD CONSTRAINT `FK_F93C6D55AF5E5B3C` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `manager_role`
--
ALTER TABLE `manager_role`
  ADD CONSTRAINT `FK_51602813783E3463` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`),
  ADD CONSTRAINT `FK_51602813D60322AC` FOREIGN KEY (`role_id`) REFERENCES `dictionary_item` (`id`);

--
-- Ograniczenia dla tabeli `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `FK_D499BFF630602CA9` FOREIGN KEY (`kind_id`) REFERENCES `dictionary_item` (`id`),
  ADD CONSTRAINT `FK_D499BFF636ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  ADD CONSTRAINT `FK_D499BFF6AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `dictionary_item` (`id`);

--
-- Ograniczenia dla tabeli `summary`
--
ALTER TABLE `summary`
  ADD CONSTRAINT `FK_CE28666336ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  ADD CONSTRAINT `FK_CE2866639B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`);

--
-- Ograniczenia dla tabeli `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `FK_723705D136ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  ADD CONSTRAINT `FK_723705D13D865311` FOREIGN KEY (`planning_id`) REFERENCES `planning` (`id`),
  ADD CONSTRAINT `FK_723705D19B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `FK_723705D1DA6A219` FOREIGN KEY (`place_id`) REFERENCES `dictionary_item` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
