-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: financial-diary
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kind_id` int NOT NULL,
  `manager_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7D3656A430602CA9` (`kind_id`),
  KEY `IDX_7D3656A4783E3463` (`manager_id`),
  CONSTRAINT `FK_7D3656A430602CA9` FOREIGN KEY (`kind_id`) REFERENCES `dictionary_item` (`id`),
  CONSTRAINT `FK_7D3656A4783E3463` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,1,2,'cash','cash');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_history`
--

DROP TABLE IF EXISTS `account_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `account_id` int DEFAULT NULL,
  `budget_id` int DEFAULT NULL,
  `current_value` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EE9164039B6B5FBA` (`account_id`),
  KEY `IDX_EE91640336ABA6B8` (`budget_id`),
  CONSTRAINT `FK_EE91640336ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  CONSTRAINT `FK_EE9164039B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_history`
--

LOCK TABLES `account_history` WRITE;
/*!40000 ALTER TABLE `account_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget`
--

DROP TABLE IF EXISTS `budget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `budget` (
  `id` int NOT NULL AUTO_INCREMENT,
  `manager_id` int NOT NULL,
  `start_date` datetime NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:dateinterval)',
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_73F2F77B783E3463` (`manager_id`),
  CONSTRAINT `FK_73F2F77B783E3463` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget`
--

LOCK TABLES `budget` WRITE;
/*!40000 ALTER TABLE `budget` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cash_set`
--

DROP TABLE IF EXISTS `cash_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cash_set` (
  `id` int NOT NULL AUTO_INCREMENT,
  `account_id` int NOT NULL,
  `budget_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B8489FB99B6B5FBA` (`account_id`),
  KEY `IDX_B8489FB936ABA6B8` (`budget_id`),
  CONSTRAINT `FK_B8489FB936ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  CONSTRAINT `FK_B8489FB99B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cash_set`
--

LOCK TABLES `cash_set` WRITE;
/*!40000 ALTER TABLE `cash_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `cash_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cash_set_entry`
--

DROP TABLE IF EXISTS `cash_set_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cash_set_entry` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cash_set_id` int NOT NULL,
  `face_value_id` int NOT NULL,
  `value` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A675CEA22105F60` (`cash_set_id`),
  KEY `IDX_5A675CEADD84B607` (`face_value_id`),
  CONSTRAINT `FK_5A675CEA22105F60` FOREIGN KEY (`cash_set_id`) REFERENCES `cash_set` (`id`),
  CONSTRAINT `FK_5A675CEADD84B607` FOREIGN KEY (`face_value_id`) REFERENCES `dictionary_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cash_set_entry`
--

LOCK TABLES `cash_set_entry` WRITE;
/*!40000 ALTER TABLE `cash_set_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `cash_set_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dictionary`
--

DROP TABLE IF EXISTS `dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dictionary` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dictionary`
--

LOCK TABLES `dictionary` WRITE;
/*!40000 ALTER TABLE `dictionary` DISABLE KEYS */;
INSERT INTO `dictionary` VALUES ('account.kind','account.kind',NULL),('budget.duration','budget.duration',NULL),('cash_set_entry.face_value','cash_set_entry.face_value',NULL),('manager.role','manager.role',NULL),('planning.department','planning.department',NULL),('planning.kind','planning.kind',NULL);
/*!40000 ALTER TABLE `dictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dictionary_item`
--

DROP TABLE IF EXISTS `dictionary_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dictionary_item` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dictionary_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F93C6D55AF5E5B3C1F1B251E` (`dictionary_id`,`item`),
  KEY `IDX_F93C6D55AF5E5B3C` (`dictionary_id`),
  CONSTRAINT `FK_F93C6D55AF5E5B3C` FOREIGN KEY (`dictionary_id`) REFERENCES `dictionary` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dictionary_item`
--

LOCK TABLES `dictionary_item` WRITE;
/*!40000 ALTER TABLE `dictionary_item` DISABLE KEYS */;
INSERT INTO `dictionary_item` VALUES (1,'account.kind','cash','cash',NULL),(2,'budget.duration','30 day','30 day',NULL),(3,'manager.role','ROLE_USER','ROLE_USER',NULL),(4,'manager.role','ROLE_ADMIN','ROLE_ADMIN',NULL),(5,'cash_set_entry.face_value','ONE_GROSS','ONE_GROSS','{\"value\": 0.01}'),(6,'cash_set_entry.face_value','TWO_GROSS','TWO_GROSS','{\"value\": 0.02}'),(7,'cash_set_entry.face_value','FIVE_GROSS','FIVE_GROSS','{\"value\": 0.05}'),(8,'cash_set_entry.face_value','TEN_GROSS','TEN_GROSS','{\"value\": 0.1}'),(9,'cash_set_entry.face_value','TWENTY_GROSS','TWENTY_GROSS','{\"value\": 0.2}'),(10,'cash_set_entry.face_value','FIFTY_GROSS','FIFTY_GROSS','{\"value\": 0.5}'),(11,'cash_set_entry.face_value','ONE_ZLOTY','ONE_ZLOTY','{\"value\": 1.0}'),(12,'cash_set_entry.face_value','TWO_ZLOTY','TWO_ZLOTY','{\"value\": 2.0}'),(13,'cash_set_entry.face_value','FIFE_ZLOTY','FIFE_ZLOTY','{\"value\": 5.0}'),(14,'cash_set_entry.face_value','TEN_ZLOTY','TEN_ZLOTY','{\"value\": 10.0}'),(15,'cash_set_entry.face_value','TWENTY_ZLOTY','TWENTY_ZLOTY','{\"value\": 20.0}'),(16,'cash_set_entry.face_value','FIFTY_ZLOTY','FIFTY_ZLOTY','{\"value\": 50.0}'),(17,'cash_set_entry.face_value','HUNDRED_ZLOTY','HUNDRED_ZLOTY','{\"value\": 100.0}'),(18,'cash_set_entry.face_value','TWOHUNDRED_ZLOTY','TWOHUNDRED_ZLOTY','{\"value\": 200.0}'),(19,'planning.kind','expense','expense',NULL),(20,'planning.kind','mixed','mixed',NULL),(21,'planning.kind','income','income',NULL);
/*!40000 ALTER TABLE `dictionary_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manager` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cash_account_id` int DEFAULT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_FA2425B9F85E0677` (`username`),
  KEY `IDX_FA2425B92F8556B0` (`cash_account_id`),
  CONSTRAINT `FK_FA2425B92F8556B0` FOREIGN KEY (`cash_account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (2,1,'admin','$2y$13$RlpkR2OXHzcywai6qon7HOmXx5d38XeAN78WCm7a8cliONiqBsHzK');
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager_role`
--

DROP TABLE IF EXISTS `manager_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manager_role` (
  `manager_id` int NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`manager_id`,`role_id`),
  KEY `IDX_51602813783E3463` (`manager_id`),
  KEY `IDX_51602813D60322AC` (`role_id`),
  CONSTRAINT `FK_51602813783E3463` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`),
  CONSTRAINT `FK_51602813D60322AC` FOREIGN KEY (`role_id`) REFERENCES `dictionary_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_role`
--

LOCK TABLES `manager_role` WRITE;
/*!40000 ALTER TABLE `manager_role` DISABLE KEYS */;
INSERT INTO `manager_role` VALUES (2,3);
/*!40000 ALTER TABLE `manager_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planning`
--

DROP TABLE IF EXISTS `planning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planning` (
  `id` int NOT NULL AUTO_INCREMENT,
  `budget_id` int NOT NULL,
  `kind_id` int NOT NULL,
  `department_id` int NOT NULL,
  `planned` double NOT NULL DEFAULT '0',
  `remained` double NOT NULL DEFAULT '0',
  `used` double NOT NULL DEFAULT '0',
  `prepaid` tinyint(1) NOT NULL DEFAULT '0',
  `closed_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D499BFF636ABA6B830602CA9AE80F5DF` (`budget_id`,`kind_id`,`department_id`),
  KEY `IDX_D499BFF636ABA6B8` (`budget_id`),
  KEY `IDX_D499BFF630602CA9` (`kind_id`),
  KEY `IDX_D499BFF6AE80F5DF` (`department_id`),
  CONSTRAINT `FK_D499BFF630602CA9` FOREIGN KEY (`kind_id`) REFERENCES `dictionary_item` (`id`),
  CONSTRAINT `FK_D499BFF636ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  CONSTRAINT `FK_D499BFF6AE80F5DF` FOREIGN KEY (`department_id`) REFERENCES `dictionary_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planning`
--

LOCK TABLES `planning` WRITE;
/*!40000 ALTER TABLE `planning` DISABLE KEYS */;
/*!40000 ALTER TABLE `planning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `summary`
--

DROP TABLE IF EXISTS `summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `summary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `budget_id` int NOT NULL,
  `account_id` int NOT NULL,
  `total_incomes` double NOT NULL,
  `total_outcomes` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_CE28666336ABA6B8` (`budget_id`),
  UNIQUE KEY `UNIQ_CE2866639B6B5FBA` (`account_id`),
  CONSTRAINT `FK_CE28666336ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  CONSTRAINT `FK_CE2866639B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `summary`
--

LOCK TABLES `summary` WRITE;
/*!40000 ALTER TABLE `summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget_id` int NOT NULL,
  `planning_id` int DEFAULT NULL,
  `place_id` int NOT NULL,
  `account_id` int NOT NULL,
  `trans_date` datetime NOT NULL,
  `account_date` datetime DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quote` decimal(10,2) NOT NULL,
  `after_transaction` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `IDX_723705D136ABA6B8` (`budget_id`),
  KEY `IDX_723705D13D865311` (`planning_id`),
  KEY `IDX_723705D1DA6A219` (`place_id`),
  KEY `IDX_723705D19B6B5FBA` (`account_id`),
  CONSTRAINT `FK_723705D136ABA6B8` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
  CONSTRAINT `FK_723705D13D865311` FOREIGN KEY (`planning_id`) REFERENCES `planning` (`id`),
  CONSTRAINT `FK_723705D19B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_723705D1DA6A219` FOREIGN KEY (`place_id`) REFERENCES `dictionary_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-09 12:40:33
