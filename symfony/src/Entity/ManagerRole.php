<?php

namespace App\Entity;

use App\Enums\Dictionaries;
use App\Repository\ManagerRoleRepository;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\DictionaryBundle\Constraints\BelongToDictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ManagerRoleRepository::class)
 */
class ManagerRole extends AbstractEntity
{

    protected $id;

    /**
     * @var Manager|null
     * @ORM\ManyToOne(targetEntity=Manager::class, inversedBy="roles")
     * @Assert\NotBlank()
     * @ORM\Id()
     */
    private $manager;

    /**
     * @var DictionaryItem|null
     * @ORM\OneToOne(targetEntity=DictionaryItem::class)
     * @Assert\NotBlank()
     * @ORM\Id()
     * @BelongToDictionary(dictionary=Dictionaries::MANAGER_ROLE)
     *
     */
    private $role;

    public function getManager(): ?Manager
    {
        return $this->manager;
    }

    public function getRole(): ?DictionaryItem
    {
        return $this->role;
    }

    public function setManager(Manager $manager): self
    {
        $this->manager = $manager;
        return $this;
    }

    public function setRole(DictionaryItem $role): self
    {
        $this->role = $role;
        return $this;
    }
}
