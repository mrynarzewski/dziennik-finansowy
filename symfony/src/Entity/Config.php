<?php


namespace App\Entity;

use App\Repository\ConfigRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\DictionaryBundle\Constraints\BelongToDictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use App\Enums\Dictionaries;

/**
 * @ORM\Entity(repositoryClass=ConfigRepository::class)
 */
class Config extends AbstractEntity
{

    /**
     * @var string|null
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @var mixed|null
     * @ORM\Column(type="json")
     */
    private $value;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed|null $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }
}