<?php

namespace App\Entity;

use App\Repository\AccountHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AccountHistoryRepository::class)
 */
class AccountHistory extends AbstractEntity
{
    /**
     * @var Account|null
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="history")
     */
    private $account;

    /**
     * @var Budget|null
     * @ORM\ManyToOne(targetEntity=Budget::class)
     */
    private $budget;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $currentValue;

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;
        return $this;
    }

    public function getCurrentValue(): ?float
    {
        return $this->currentValue;
    }

    public function setCurrentValue(float $currentValue): self
    {
        $this->currentValue = $currentValue;

        return $this;
    }

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(?Budget $budget): self
    {
        $this->budget = $budget;
        return $this;
    }
}
