<?php

namespace App\Entity;

use App\Repository\SummaryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SummaryRepository::class)
 */
class Summary extends AbstractEntity
{

    /**
     * @var Budget|null
     * @ORM\OneToOne(targetEntity=Budget::class, inversedBy="summary")
     * @ORM\JoinColumn(nullable=false)
     */
    private $budget;

    /**
     * @var Account|null
     * @ORM\OneToOne(targetEntity=Account::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $totalIncomes = 0;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $totalOutcomes = 0;

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(?Budget $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getTotalIncomes(): ?float
    {
        return $this->totalIncomes;
    }

    public function setTotalIncome(float $totalIncomes): self
    {
        $this->totalIncomes = $totalIncomes;

        return $this;
    }

    public function getTotalOutcomes(): ?float
    {
        return $this->totalOutcomes;
    }

    public function setTotalOutcomes(float $totalOutcomes): self
    {
        $this->totalOutcomes = $totalOutcomes;

        return $this;
    }
}
