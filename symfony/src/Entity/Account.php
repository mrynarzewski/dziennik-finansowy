<?php

namespace App\Entity;

use App\Enums\Dictionaries;
use App\Repository\AccountRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\DictionaryBundle\Constraints\BelongToDictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 */
class Account extends AbstractEntity
{
    /**
     * @var DictionaryItem|null
     * @ORM\ManyToOne(targetEntity=DictionaryItem::class)
     * @ORM\JoinColumn(nullable=false)
     * @BelongToDictionary(dictionary=Dictionaries::ACCOUNT_KIND)
     */
    private $kind;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var Manager|null
     * @ORM\ManyToOne(targetEntity=Manager::class, inversedBy="accounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $manager;

    /**
     * @var Collection|AccountHistory[]
     * @ORM\OneToMany(targetEntity=AccountHistory::class, mappedBy="account")
     */
    private $history;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $reference;

    public function __construct()
    {
        $this->history = new ArrayCollection();
    }

    public function getKind(): ?DictionaryItem
    {
        return $this->kind;
    }

    public function setKind(DictionaryItem $kind): self
    {
        $this->kind = $kind;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getManager(): ?Manager
    {
        return $this->manager;
    }

    public function setManager(Manager $manager): self
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * @return Collection|AccountHistory[]
     */
    public function getHistory(): Collection
    {
        return $this->history;
    }

    /**
     * @return string|null
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param string|null $reference
     * @return Account
     */
    public function setReference(?string $reference): Account
    {
        $this->reference = $reference;
        return $this;
    }
}
