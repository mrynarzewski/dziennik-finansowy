<?php

namespace App\Entity;

use App\Repository\BudgetRepository;
use DateInterval;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BudgetRepository::class)
 */
class Budget extends AbstractEntity
{

    /**
     * @var Manager|null
     * @ORM\ManyToOne(targetEntity=Manager::class, inversedBy="budgets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $manager;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @var DateInterval
     * @ORM\Column(type="dateinterval", nullable=true)
     */
    private $duration;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var Collection|Planning[]
     * @ORM\OneToMany(targetEntity=Planning::class, mappedBy="budget")
     */
    private $plannings;

    /**
     * @var Collection|Transaction[]
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="budget", orphanRemoval=true)
     */
    private $transactions;

    /**
     * @var Summary|null
     * @ORM\OneToOne(targetEntity=Summary::class, mappedBy="budget", orphanRemoval=true)
     */
    private $summary;

    public function __construct()
    {
        $this->plannings = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function getManager(): ?Manager
    {
        return $this->manager;
    }

    public function setManager(?Manager $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getDuration(): ?DateInterval
    {
        return $this->duration;
    }

    public function setDuration(DateInterval $duration): self
    {
        $this->duration = $duration;
        $this->setEndDate($this->getEndDate()->setTime(23, 59, 59));

        return $this;
    }

    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @return Collection|Planning[]
     */
    public function getPlannings(): Collection
    {
        return $this->plannings;
    }

    public function addPlanning(Planning $planning): self
    {
        if (!$this->plannings->contains($planning)) {
            $this->plannings[] = $planning;
            $planning->setBudget($this);
        }

        return $this;
    }

    public function removePlanning(Planning $planning): self
    {
        if ($this->plannings->removeElement($planning)) {
            // set the owning side to null (unless already changed)
            if ($planning->getBudget() === $this) {
                $planning->setBudget(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setBudget($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getBudget() === $this) {
                $transaction->setBudget(null);
            }
        }

        return $this;
    }

    /**
     * @return Summary|null
     */
    public function getSummary(): ?Summary
    {
        return $this->summary;
    }

    /**
     * @param Summary|null $summary
     * @return Budget
     */
    public function setSummary(?Summary $summary): self
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @param DateTime $endDate
     * @return Budget
     */
    public function setEndDate(DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }
}
