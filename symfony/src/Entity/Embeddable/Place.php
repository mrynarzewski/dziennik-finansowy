<?php

namespace App\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class Place
{
    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $dictionaryItem;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $custom;

    /**
     * @return int|null
     */
    public function getDictionaryItem(): ?int
    {
        return $this->dictionaryItem;
    }

    /**
     * @param int|null $dictionaryItem
     * @return Place
     */
    public function setDictionaryItem(?int $dictionaryItem): Place
    {
        $this->dictionaryItem = $dictionaryItem;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustom(): ?string
    {
        return $this->custom;
    }

    /**
     * @param string|null $custom
     * @return Place
     */
    public function setCustom(?string $custom): Place
    {
        $this->custom = $custom;
        return $this;
    }


}