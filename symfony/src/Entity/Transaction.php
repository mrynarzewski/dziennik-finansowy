<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction extends AbstractEntity
{

    protected $id;

    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;

    /**
     * @var Budget|null
     * @ORM\ManyToOne(targetEntity=Budget::class, inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $budget;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $transDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $accountDate;

    /**
     * @var Planning|null
     * @ORM\ManyToOne(targetEntity=Planning::class, inversedBy="transactions")
     * @ORM\JoinColumn(nullable=true)
     */
    private $planning;

    /**
     * @var DictionaryItem|null
     * @ORM\ManyToOne(targetEntity=DictionaryItem::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $place;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1000)
     */
    private $description;

    /**
     * @var string|null
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $quote;

    /**
     * @var string|null
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     * Calculated
     */
    private $afterTransaction;

    /**
     * @var Account|null
     * @ORM\ManyToOne(targetEntity=Account::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    public function getUid(): ?int
    {
        return $this->uid;
    }

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(?Budget $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getTransDate(): ?\DateTimeInterface
    {
        return $this->transDate;
    }

    public function setTransDate(\DateTimeInterface $transDate): self
    {
        $this->transDate = $transDate;

        return $this;
    }

    public function getAccountDate(): ?\DateTime
    {
        return $this->accountDate;
    }

    public function setAccountDate(?\DateTime $accountDate): self
    {
        $this->accountDate = $accountDate;

        return $this;
    }

    public function getPlanning(): ?Planning
    {
        return $this->planning;
    }

    public function setPlanning(?Planning $planning): self
    {
        $this->planning = $planning;

        return $this;
    }

    public function getPlace(): ?DictionaryItem
    {
        return $this->place;
    }

    public function setPlace(?DictionaryItem $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    public function setQuote(string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getAfterTransaction(): ?string
    {
        return $this->afterTransaction;
    }

    public function setAfterTransaction(?string $afterTransaction): self
    {
        $this->afterTransaction = $afterTransaction;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }
}
