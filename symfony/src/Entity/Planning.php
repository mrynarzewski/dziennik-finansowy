<?php

namespace App\Entity;

use App\Repository\PlanningRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\DictionaryBundle\Constraints\BelongToDictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

/**
 * @ORM\Entity(repositoryClass=PlanningRepository::class)
 * @ORM\Table(
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(columns={"budget_id", "kind_id", "department_id"})
 *     }
 * )
 */
class Planning extends AbstractEntity
{

    /**
     * @var Budget|null
     * @ORM\ManyToOne(targetEntity=Budget::class, inversedBy="plannings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $budget;

    /**
     * @var DictionaryItem|null
     * @ORM\ManyToOne(targetEntity=DictionaryItem::class)
     * @ORM\JoinColumn(nullable=false)
     * @BelongToDictionary(dictionary=App\Enums\Dictionaries::PLANNING_KIND)
     */
    private $kind;

    /**
     * @var DictionaryItem|null
     * @ORM\ManyToOne(targetEntity=DictionaryItem::class)
     * @ORM\JoinColumn(nullable=false)
     * @BelongToDictionary(dictionary=App\Enums\Dictionaries::PLANNING_DEPARTMENT)
     */
    private $department;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false, options={"default":0})
     */
    private $planned = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false, options={"default":0})
     */
    private $remained = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=false, options={"default":0})
     */
    private $used = 0;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    private $prepaid = false;

    /**
     * @var Collection|Transaction[]
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="planning")
     */
    private $transactions;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $closedAt;

    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getKind(): ?DictionaryItem
    {
        return $this->kind;
    }

    public function setKind(?DictionaryItem $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    public function getDepartment(): ?DictionaryItem
    {
        return $this->department;
    }

    /**
     * @param DictionaryItem|null $department
     * @return self
     */
    public function setDepartment(?DictionaryItem $department): self
    {
        $this->department = $department;
        return $this;
    }

    /**
     * @return float
     */
    public function getPlanned(): ?float
    {
        return $this->planned;
    }

    /**
     * @param float $planned
     * @return self
     */
    public function setPlanned(float $planned): self
    {
        $this->planned = $planned;
        return $this;
    }

    /**
     * @return float
     */
    public function getRemained(): ?float
    {
        return $this->remained;
    }

    /**
     * @param float $remained
     * @return self
     */
    public function setRemained(float $remained): self
    {
        $this->remained = $remained;
        return $this;
    }

    /**
     * @return float
     */
    public function getUsed(): ?float
    {
        return $this->used;
    }

    /**
     * @param float $used
     * @return self
     */
    public function setUsed(float $used): self
    {
        $this->used = $used;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPrepaid(): ?bool
    {
        return $this->prepaid;
    }

    /**
     * @param bool $prepaid
     * @return self
     */
    public function setPrepaid(bool $prepaid): self
    {
        $this->prepaid = $prepaid;
        return $this;
    }

    /**
     * @return Budget|null
     */
    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    /**
     * @param Budget|null $budget
     * @return $this
     */
    public function setBudget(?Budget $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setPlanning($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getPlanning() === $this) {
                $transaction->setPlanning(null);
            }
        }

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosedAt(): ?\DateTime
    {
        return $this->closedAt;
    }

    /**
     * @param \DateTime|null $closedAt
     * @return Planning
     */
    public function setClosedAt(?\DateTime $closedAt): self
    {
        $this->closedAt = $closedAt;
        return $this;
    }
}
