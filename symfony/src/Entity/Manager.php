<?php

namespace App\Entity;

use App\Repository\ManagerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=ManagerRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class Manager extends AbstractEntity implements UserInterface
{
    /**
     * @var string
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @var Collection|ManagerRole[]
     * @ORM\OneToMany  (targetEntity=ManagerRole::class, mappedBy="manager")
     */
    private $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var Collection|Budget[]
     * @ORM\OneToMany(targetEntity=Budget::class, mappedBy="manager")
     */
    private $budgets;

    /**
     * @var Collection|Account[]
     * @ORM\OneToMany(targetEntity=Account::class, mappedBy="manager")
     */
    private $accounts;

    /**
     * @var Account|null
     * @ORM\ManyToOne(targetEntity=Account::class)
     */
    private $cashAccount;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->budgets = new ArrayCollection();
        $this->accounts = new ArrayCollection();
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $managerRoles = $this->getManagerRoles();
        $result = [];
        foreach ($managerRoles as $managerRole) {
            $result[] = $managerRole->getRole()->getItem();
        }

        return $result;
    }

    /**
     * @return Collection|ManagerRole[]
     */
    public function getManagerRoles(): Collection
    {
        return $this->roles;
    }

    public function addManagerRole(ManagerRole $managerRole): self
    {
        if (!$this->roles->contains($managerRole)) {
            $this->roles[] = $managerRole;
            $managerRole->setManager($this);
        }

        return $this;
    }

    public function removeManagerRole(ManagerRole $managerRole): self
    {
        if ($this->roles->removeElement($managerRole)) {
            $managerRole->setManager(null);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSalt()
    {

    }

    /**
     * @inheritdoc
     */
    public function eraseCredentials()
    {

    }

    /**
     * @return Collection|Budget[]
     */
    public function getBudgets(): Collection
    {
        return $this->budgets;
    }

    public function addBudget(Budget $budget): self
    {
        if (!$this->budgets->contains($budget)) {
            $this->budgets[] = $budget;
            $budget->setManager($this);
        }

        return $this;
    }

    public function removeBudget(Budget $budget): self
    {
        if ($this->budgets->removeElement($budget)) {
            // set the owning side to null (unless already changed)
            if ($budget->getManager() === $this) {
                $budget->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    public function addAccount(Account $account): self
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts[] = $account;
            $account->setManager($this);
        }

        return $this;
    }

    public function removeAccount(Account $account): self
    {
        if ($this->accounts->removeElement($account)) {
            // set the owning side to null (unless already changed)
            if ($account->getManager() === $this) {
                $account->setManager(null);
            }
        }

        return $this;
    }

    /**
     * @return Account|null
     */
    public function getCashAccount(): ?Account
    {
        return $this->cashAccount;
    }

    /**
     * @param Account|null $cashAccount
     * @return Manager
     */
    public function setCashAccount(?Account $cashAccount): Manager
    {
        $this->cashAccount = $cashAccount;
        return $this;
    }
}
