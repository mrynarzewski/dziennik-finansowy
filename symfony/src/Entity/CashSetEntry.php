<?php

namespace App\Entity;

use App\Enums\Dictionaries;
use App\Repository\CashSetEntryRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mrynarzewski\DictionaryBundle\Constraints\BelongToDictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

/**
 * @ORM\Entity(repositoryClass=CashSetEntryRepository::class)
 */
class CashSetEntry extends AbstractEntity
{
    /**
     * @var Collection|CashSet
     * @ORM\ManyToOne(targetEntity=CashSet::class, inversedBy="entries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cashSet;

    /**
     * @var DictionaryItem|null
     * @ORM\ManyToOne(targetEntity=DictionaryItem::class)
     * @ORM\JoinColumn(nullable=false)
     * @BelongToDictionary(dictionary=Dictionaries::CASHSETENTRY_FACEVALUE)
     */
    private $faceValue;

    /**
     * @var float|null
     * @ORM\Column(type="integer")
     */
    private $value;

    public function getCashSet(): ?CashSet
    {
        return $this->cashSet;
    }

    public function setCashSet(?CashSet $cashSet): self
    {
        $this->cashSet = $cashSet;

        return $this;
    }

    public function getFaceValue(): ?DictionaryItem
    {
        return $this->faceValue;
    }

    public function setFaceValue(?DictionaryItem $faceValue): self
    {
        $this->faceValue = $faceValue;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
