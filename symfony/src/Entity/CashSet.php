<?php

namespace App\Entity;

use App\Repository\CashSetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CashSetRepository::class)
 */
class CashSet extends AbstractEntity
{
    /**
     * @var Account|null
     * @ORM\ManyToOne(targetEntity=Account::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $account;

    /**
     * @var Budget|null
     * @ORM\ManyToOne(targetEntity="Budget")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $budget;

    /**
     * @var Collection|CashSetEntry[]
     * @ORM\OneToMany(targetEntity=CashSetEntry::class, mappedBy="cashSet", orphanRemoval=true)
     */
    private $entries;

    public function __construct()
    {
        $this->entries = new ArrayCollection();
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(Budget $budget): self
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return Collection|CashSetEntry[]
     */
    public function getEntries(): Collection
    {
        return $this->entries;
    }

    public function addEntry(CashSetEntry $entry): self
    {
        if (!$this->entries->contains($entry)) {
            $this->entries[] = $entry;
            $entry->setCashSet($this);
        }

        return $this;
    }

    public function removeEntry(CashSetEntry $entry): self
    {
        if ($this->entries->removeElement($entry)) {
            // set the owning side to null (unless already changed)
            if ($entry->getCashSet() === $this) {
                $entry->setCashSet(null);
            }
        }

        return $this;
    }
}
