<?php

namespace App\Enums;

use MabeEnum\Enum;

class CashEntryFaceValue extends Enum
{
    const ONE_GROSS = 0.01;
    const TWO_GROSS = 0.02;
    const FIVE_GROSS = 0.05;
    const TEN_GROSS = 0.1;
    const TWENTY_GROSS = 0.2;
    const FIFTY_GROSS = 0.5;
    const ONE_ZLOTY = 1.0;
    const TWO_ZLOTY = 2.0;
    const FIFE_ZLOTY = 5.0;
    const TEN_ZLOTY = 10.0;
    const TWENTY_ZLOTY = 20.0;
    const FIFTY_ZLOTY = 50.0;
    const HUNDRED_ZLOTY = 100.0;
    const TWOHUNDRED_ZLOTY = 200.0;

    public static function getDictionary(): string
    {
        return Dictionaries::CASHSETENTRY_FACEVALUE;
    }

    public static function process(Enum $enum): array
    {
        $item = $enum->getName();
        $description = $item;
        $extra = [
            'value' => $enum->getValue(),
        ];

        return [$item, $description, $extra];
    }
}