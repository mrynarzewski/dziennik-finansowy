<?php

namespace App\Enums;

use MabeEnum\Enum;

class Dictionaries extends Enum
{
    public const MANAGER_ROLE = 'manager.role';
    public const ACCOUNT_KIND = 'account.kind';
    public const BUDGET_DURATION = 'budget.duration';
    public const PLANNING_KIND = 'planning.kind';
    public const PLANNING_DEPARTMENT = 'planning.department';
    public const CASHSETENTRY_FACEVALUE = 'cash_set_entry.face_value';
}