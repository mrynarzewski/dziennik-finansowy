<?php

namespace App\Enums;

use MabeEnum\Enum;

class ManagerRole extends Enum
{
    public static function getDictionary(): string
    {
        return Dictionaries::MANAGER_ROLE;
    }

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    public static function process(Enum $enum): array
    {
        $item = $enum->getValue();
        $description = $item;
        $extra = null;

        return [$item, $description, $extra];
    }
}