<?php

namespace App\Enums;

use MabeEnum\Enum;

class PlanningKind extends Enum
{
    const EXPENSE = 'expense';
    const MIXED = 'mixed';
    const INCOME = 'income';

    public static function getDictionary(): string
    {
        return Dictionaries::PLANNING_KIND;
    }

    public static function process(Enum $enum): array
    {
        $item = $enum->getValue();
        $description = $item;
        $extra = null;

        return [$item, $description, $extra];
    }
}