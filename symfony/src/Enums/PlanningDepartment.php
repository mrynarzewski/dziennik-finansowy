<?php

namespace App\Enums;

use MabeEnum\Enum;

class PlanningDepartment extends Enum
{
    public static function getDictionary(): string
    {
        return Dictionaries::PLANNING_DEPARTMENT;
    }

    public static function process(Enum $enum): array
    {
        $item = $enum->getValue();
        $description = $item;
        $extra = null;

        return [$item, $description, $extra];
    }
}