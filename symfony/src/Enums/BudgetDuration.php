<?php

namespace App\Enums;

use MabeEnum\Enum;

class BudgetDuration extends Enum
{
    public static function getDictionary(): string
    {
        return Dictionaries::BUDGET_DURATION;
    }

    public const THIRTY_DAY = '30 day';

    public static function process(Enum $enum): array
    {
        $item = $enum->getValue();
        $description = $item;
        $extra = null;

        return [$item, $description, $extra];
    }
}