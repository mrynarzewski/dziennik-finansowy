<?php

namespace App\Enums;

use MabeEnum\Enum;

class AccountKind extends Enum
{
    const CASH = 'cash';
    const OTHER = 'other';

    public static function getDictionary(): string
    {
        return Dictionaries::ACCOUNT_KIND;
    }
}