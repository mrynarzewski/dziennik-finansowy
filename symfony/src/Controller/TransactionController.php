<?php


namespace App\Controller;

use App\Entity\Budget;
use App\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manage/budget/{budget}/transaction", name="app_manage_transaction_")
 */
class TransactionController extends AbstractController
{

    /**
     * @Route("/", name="list", methods={"GET"})
     */
    public function index(Budget $budget): Response
    {
        $transactionRepository = $this->getDoctrine()->getRepository(Transaction::class);
        return $this->render('general/index.html.twig', [
            'itemName' => 'Transaction',
            'items' => $transactionRepository->findByBudget($budget),
            'budget' => $budget,
            'templateHeader' => 'transaction/header',
            'templateBody=' => 'transaction/body',
            'propertyLength' => 9,
        ]);
    }

}