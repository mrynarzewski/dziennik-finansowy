<?php

namespace App\Controller;

use App\Service\Traits\EntityManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryItemRepositoryAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dictionary-item")
 */
class DictionaryItemController extends AbstractController
{

    use DictionaryItemRepositoryAwareTrait;
    use EntityManagerAwareTrait;

    /**
     * @Route("/{dictionary}", methods={"GET"}, name="app_dictionaryItem_list")
     * @param Dictionary $dictionary
     * @param Request $request
     * @return JsonResponse
     */
    public function getItemsAction(Dictionary $dictionary, Request $request): JsonResponse
    {
        $repository = $this->getDoctrine();
        $repository = $repository->getRepository(DictionaryItem::class);
        $searchItem = $request->get('q');
        $items = $repository->getItemsFromDictionary($dictionary, $searchItem);
        $responses = [];
        foreach ($items as $item) {
            $responses[] = [
                'id' => $item->getId(),
                'item' => $item->getItem(),
                'description' => $item->getDescription(),
                'text' => $item->getItem(),
                'extra' => $item->getExtra(),
            ];
        }
        return new JsonResponse(['results' => $responses]);
    }

    /**
     * @Route("/{dictionary}/to-select", methods={"GET"}, name="app_dictionaryItem_listToSelect")
     * @param Dictionary $dictionary
     * @param Request $request
     * @return JsonResponse
     */
    public function getItemsToSelect(Dictionary $dictionary, Request $request): JsonResponse
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(DictionaryItem::class);
        $searchItem = $request->get('q');
        $textProperty = $request->get('text_property', 'item');
        $getter = 'get'.ucfirst($textProperty);
        $items = $repository->getItemsFromDictionary($dictionary, $searchItem);
        $responses = [];
        foreach ($items as $item) {
            $response = [
                'id' => $item->getId(),
                'text' => $item->$getter(),
            ];
            $responses[] = $response;
        }
        return new JsonResponse(['results' => $responses]);
    }

    /**
     * @Route(path="/{dictionary}", name="app_dictionaryItem_add", methods={"POST"})
     * @param Dictionary $dictionary
     * @param Request $request
     * @return JsonResponse
     */
    public function addDictionaryItem(Dictionary $dictionary, Request $request): JsonResponse
    {
        $item =  $request->request->get('item');
        $dictionaryItem = $this->dictionaryItemRepository->findOneBy([
            'dictionary' => $dictionary,
            'item' => $item,
        ]);
        if (!is_null($dictionaryItem)) {
            return $this->json([
                'id' => $dictionaryItem->getId(),
                'text' => $dictionaryItem->getItem(),
            ], JsonResponse::HTTP_OK);
        }
        $dictionaryItem = new DictionaryItem();
        $dictionaryItem->setDictionary($dictionary);
        $dictionaryItem->setItem($item);
        $dictionaryItem->setDescription($request->request->get('description'));
        $dictionaryItem->setExtra($request->request->get('extra'));
        $this->entityManager->persist($dictionaryItem);
        $this->entityManager->flush();

        return $this->json([
            'id' => $dictionaryItem->getId(),
            'text' => $dictionaryItem->getItem(),
        ], JsonResponse::HTTP_CREATED);
    }
}