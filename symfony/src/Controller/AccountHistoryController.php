<?php


namespace App\Controller;

use App\Entity\Account;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account/{account}", name="account_history_")
 */
class AccountHistoryController extends AbstractController
{

    /**
     * @Route(path="/history", name="index")
     */
    public function aAction(Account $account): Response
    {
        if ($account->getManager() !== $this->getUser()) {
            throw new AccessDeniedException();
        }
        return new Response();
    }
}