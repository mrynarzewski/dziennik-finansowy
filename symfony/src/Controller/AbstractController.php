<?php


namespace App\Controller;

use App\Entity\Manager;
use Exception;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractController extends BaseController
{
    /**
     * @return Manager|UserInterface|null
     */
    protected function getUser()
    {
        return parent::getUser();
    }

    /** @var SerializerInterface */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     * @required
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /** @var ValidatorInterface */
    private $validator;

    /**
     * @required
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /** @var RequestStack */
    private $requestStack;

    /**
     * @required
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack): void
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $content
     * @param string $class
     * @param array<int, string> $groups
     * @param string $type
     * @return mixed
     */
    protected function jmsDeserialization(string $content, string  $class, array $groups = ['Default'], string $type = 'json')
    {
        $serializerContext = DeserializationContext::create()->setGroups($groups);

        return $this->serializer->deserialize($content, $class, $type, $serializerContext);
    }

    /**
     * @param string $content
     * @param string[] $groups
     * @param bool $includeHeaders
     * @return string|null
     */
    protected function csvSerialization(string $content, array $groups = ['Default'], bool $includeHeaders = true): ?string
    {
        $json = $this->jmsSerialization($content, $groups, 'json', true);
        $json = json_decode($json, true);
        $fp = fopen('php://temp', 'w');
        if ($includeHeaders) {
            fputcsv($fp, array_keys(array_values($json)[0]));
        }
        foreach ($json as $fields) {
            fputcsv($fp, $fields);
        }
        rewind($fp);
        $csv = stream_get_contents($fp);
        fclose($fp);
        return $csv;
    }

    /**
     * @param string $content
     * @param string|null $contentType
     * @param string|null $name
     * @return Response
     */
    protected function createDownloadableResponse(string $content, ?string $contentType = 'text/plain', ?string $name = "download.txt"): Response
    {
        $response = new Response($content);
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $name . '"');
        return $response;
    }

    /**
     * @param mixed $serializationObject
     * @param string[] $groups
     * @param string $type
     * @param bool $serializeNull
     * @return string
     */
    protected function jmsSerialization($serializationObject, array $groups = ['Default'], string $type = 'json', bool $serializeNull = false): string
    {
        $serializerContext = SerializationContext::create()->setGroups($groups);

        if ($serializeNull) {
            $serializerContext->setSerializeNull(true);
        }

        return $this->serializer->serialize($serializationObject, $type, $serializerContext);
    }

    /**
     * @param string $class
     * @param string[] $groups
     * @param string|null $requestData
     * @return mixed
     * @throws Exception
     */
    protected function deserializeRequestData(string $class, array $groups = ['default'], ?string $requestData = null)
    {
        if (is_null($requestData)) {
            $requestData = $this->requestStack->getCurrentRequest()->getContent();
        }
        $responseModel = $this->jmsDeserialization(
            $requestData,
            $class,
            $groups
        );
        $groups = in_array('default', $groups, true) ? array_merge(['Default'], $groups) : $groups;
        $this->validateModel($responseModel, $groups);
        return $responseModel;
    }

    /**
     * @param mixed $responseModel
     * @param array $groups
     * @throws Exception
     */
    protected function validateModel($responseModel, array $groups): void
    {
        $errors = $this->validator->validate($responseModel, null, $groups);
        if (count($errors) > 0) {
            throw new Exception($errors[0]->getMessage());
        }
    }
}