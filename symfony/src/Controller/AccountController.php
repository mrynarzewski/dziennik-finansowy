<?php

namespace App\Controller;

use App\Entity\Account;
use App\Form\AccountImportType;
use App\Form\AccountType;
use App\Service\Traits\AccountManagerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manager/account")
 */
class AccountController extends AbstractController
{

    use AccountManagerAwareTrait;

    /**
     * @Route("/", name="account_index", methods={"GET"})
     */
    public function index(): Response
    {
        $accountRepository = $this->get('doctrine')->getRepository(Account::class);
        $user = $this->getUser();
        $accounts = $accountRepository->findByManager($user);
        $params = [
            'accounts' => $accounts,
        ];

        return $this->render('account/index.html.twig', $params);
    }

    /**
     * @Route("/new", name="account_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $account = new Account();
        $form = $this->createForm(AccountType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->accountManager->create(
                $this->getUser(),
                $form->get('kind')->getData(),
                $form->get('name')->getData(),
                $form->get('reference')->getData()
            );

            return $this->redirectToRoute('account_index');
        }

        return $this->render('account/new.html.twig', [
            'account' => $account,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/import", name="account_import", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function importAction(Request $request): Response
    {
        $form = $this->createForm(AccountImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->accountManager->import(
                $this->getUser(),
                $form->get('spreadSheetId')->getData(),
                $form->get('range')->getData()
            );

            return $this->redirectToRoute('account_index');
        }

        return $this->render('form/default.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(path="/account/{account}/close", name="account_close")
     */
    public function closeAction(Account $account): Response
    {
        $this->accountManager->close($account);
        return $this->redirectToRoute('account_index');
    }

    /**
     * @Route("/{id}", name="account_show", methods={"GET"})
     */
    public function show(Account $account): Response
    {
        return $this->render('account/show.html.twig', [
            'account' => $account,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="account_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Account $account): Response
    {
        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('account_index');
        }

        return $this->render('account/edit.html.twig', [
            'account' => $account,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="account_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Account $account): Response
    {
        if ($this->isCsrfTokenValid('delete'.$account->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($account);
            $entityManager->flush();
        }

        return $this->redirectToRoute('account_index');
    }
}
