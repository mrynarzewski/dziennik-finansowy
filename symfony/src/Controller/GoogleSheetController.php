<?php


namespace App\Controller;


use App\Service\External\GoogleSheetsImporterManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sheet")
 */
class GoogleSheetController extends AbstractController
{
    /**
     * @Route("/auth/success")
     */
    public function authSuccess(): Response
    {
        return new Response('Success');
    }

    /**
     * @Route("/cells/{spreadsheetId}")
     * @param string $spreadsheetId
     * @param Request $request
     * @param GoogleSheetsImporterManager $googleManager
     * @return Response
     */
    public function readCellsAction(string $spreadsheetId, Request $request, GoogleSheetsImporterManager $googleManager): Response
    {
        $googleManager->readCells($spreadsheetId, $request->get('range'));
        return new Response();
    }
}