<?php


namespace App\Controller\Admin;

use App\Voter\DictionaryItemVoter;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Form\DictionaryItemType;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryItemRepositoryAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/dictionaryItem", name="app_admin_dictionaryItem_")
 */
class DictionaryItemController extends AbstractController
{

    use DictionaryItemRepositoryAwareTrait;

    /**
     * @Route("/{dictionary}", name="show", methods={"GET"})
     * @param Dictionary $dictionary
     * @return Response
     */
    public function index(Dictionary $dictionary): Response
    {
        return $this->render('admin/dictionaryItem/index.html.twig', [
            'items' => $this->dictionaryItemRepository->getItemsFromDictionary($dictionary),
            'dictionary' => $dictionary,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     * @param Request $request
     * @param DictionaryItem $dictionaryItem
     * @return Response
     */
    public function edit(Request $request, DictionaryItem $dictionaryItem): Response
    {
        $this->denyAccessUnlessGranted(DictionaryItemVoter::EDIT, $dictionaryItem);
        $form = $this->createForm(DictionaryItemType::class, $dictionaryItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_admin_dictionaryItem_show', [
                'dictionary' => $dictionaryItem->getDictionary()->getId(),
            ]);
        }

        return $this->render('edit.html.twig', [
            'form' => $form->createView(),
            'params' => [
                'dictionary' => $dictionaryItem->getDictionary()->getId(),
            ],
            'BackLink' => 'app_admin_dictionaryItem_show',
            'title' => 'Edit Dictionary item',
        ]);
    }
}