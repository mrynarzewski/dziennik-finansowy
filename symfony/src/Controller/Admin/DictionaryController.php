<?php


namespace App\Controller\Admin;

use Mrynarzewski\DictionaryBundle\Repository\DictionaryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/dictionary", name="app_admin_dictionary_")
 */
class DictionaryController extends AbstractController
{

    /**
     * @Route("/", name="index", methods={"GET"})
     * @param DictionaryRepository $dictionaryRepository
     * @return Response
     */
    public function index(DictionaryRepository $dictionaryRepository): Response
    {
        return $this->render('admin/dictionary/index.html.twig', [
            'dictionaries' => $dictionaryRepository->findAll(),
        ]);
    }
}