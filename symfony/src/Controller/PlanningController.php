<?php

namespace App\Controller;

use App\Entity\Budget;
use App\Entity\Planning;
use App\Form\PlanningType;
use App\Repository\PlanningRepository;
use App\Service\Traits\PlanningManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryItemServiceAwareTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/manage/budget/{budget}/planning", name="app_manage_planning_")
 */
class PlanningController extends AbstractController
{
    use PlanningManagerAwareTrait;
    use DictionaryItemServiceAwareTrait;

    /**
     * @Route("/", name="list", methods={"GET"})
     */
    public function index(PlanningRepository $repository, Budget $budget): Response
    {
        return $this->render('planning/index.html.twig', [
            'items' => $repository->findByBudget($budget),
            'budget' => $budget,
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, Budget $budget): Response
    {
        $form = $this->createForm(PlanningType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $formData = $form->getData();
            $this->planningManager->create(
                $budget,
                $formData['kind'],
                $formData['department'],
                $formData['planned'],
                $formData['prepaid']
            );
            return $this->redirectToRoute('app_manage_planning_list', [
                'budget' => $budget->getId()
            ]);
        }

        return $this->render('planning/new.html.twig', [
            'budget' => $budget,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{planning}", name="show", methods={"GET"})
     */
    public function show(Planning $planning): Response
    {
        return $this->render('planning/show.html.twig', [
            'planning' => $planning,
        ]);
    }
}