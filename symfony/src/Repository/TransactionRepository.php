<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\Budget;
use App\Entity\Planning;
use App\Entity\Transaction;
use App\Repository\Abstracts\TransactionRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository implements TransactionRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transaction::class);
    }

    public function findByPlanning(Planning $planning): array
    {
        return [];
    }

    public function findByBudget(Budget $budget): array
    {
       $queryBuilder = $this->createQueryBuilder('t');
       $queryBuilder->andWhere('t.budget = :budget');
       $queryBuilder->setParameter('budget', $budget);
       $query = $queryBuilder->getQuery();
       return $query->getResult();
    }

    public function findByBudgetAndPlanning(Budget $budget, Planning $planning): array
    {
        return [];
    }

    public function findByBudgetAndAccount(Budget $budget, Account $account): array
    {
        return [];
    }
}
