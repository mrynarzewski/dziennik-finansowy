<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\Manager;
use App\Repository\Abstracts\AccountRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

/**
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 * @method Account|null findOneBy(array $criteria, array $orderBy = null)
 * @method Account[]    findAll()
 * @method Account[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class AccountRepository extends ServiceEntityRepository implements AccountRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    /**
     * @inheritDoc
     */
    public function findByManager(Manager $manager): array
    {
        return $this->findBy([
            'manager' => $manager,
        ]);
    }

    public function findByKind(Manager $manager, DictionaryItem $kind): array
    {
        return $this->findBy([
            'manager' => $manager,
            'kind' => $kind,
        ]);
    }

    public function findOneByName(Manager $manager, string $name): ?Account
    {
        try {
            return $this->createQueryBuilder('a')
                ->andWhere('a.manager = :manager')
                ->andWhere('a.name LIKE :name')
                ->setParameter('manager', $manager)
                ->setParameter('name', '%' . $name . '%')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}