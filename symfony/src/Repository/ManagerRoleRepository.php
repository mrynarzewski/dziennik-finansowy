<?php

namespace App\Repository;

use App\Entity\Manager;
use App\Entity\ManagerRole;
use App\Repository\Abstracts\ManagerRoleRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

/**
 * @method ManagerRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method ManagerRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method ManagerRole[]    findAll()
 * @method ManagerRole[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class ManagerRoleRepository extends ServiceEntityRepository implements ManagerRoleRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ManagerRole::class);
    }

    public function findByManager(Manager $manager): array
    {
        return $this->findBy([
            'manager' => $manager
        ]);
    }

    public function findByRole(DictionaryItem $role): array
    {
        return $this->findBy([
            'role' => $role
        ]);
    }
}