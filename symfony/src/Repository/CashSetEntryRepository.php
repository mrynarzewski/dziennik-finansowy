<?php

namespace App\Repository;

use App\Entity\CashSetEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CashSetEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method CashSetEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method CashSetEntry[]    findAll()
 * @method CashSetEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CashSetEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CashSetEntry::class);
    }
}
