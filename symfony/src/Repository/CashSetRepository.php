<?php

namespace App\Repository;

use App\Entity\CashSet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CashSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method CashSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method CashSet[]    findAll()
 * @method CashSet[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class CashSetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CashSet::class);
    }
}