<?php

namespace App\Repository;

use App\Entity\Config;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use http\Exception\InvalidArgumentException;

/**
 * @method Config|null find($id, $lockMode = null, $lockVersion = null)
 * @method Config|null findOneBy(array $criteria, array $orderBy = null)
 * @method Config[]    findAll()
 * @method Config[]    findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Config::class);
    }

    public function has(string $name): bool
    {
        return !empty($this->createQueryBuilder('c')
            ->where('c.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult());
    }

    public function get(string $name)
    {
        /** @var Config|null $config */
        $config = $this->createQueryBuilder('c')
            ->where('c.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
        if (empty($config)) {
            return null;
        }
        return $config->getValue();
    }

    public function createItem(string $name, $value): void
    {
        if ($this->has($name)) {
            throw new \InvalidArgumentException('Config name already exists');
        }
        $config = new Config();
        $config->setName($name);
        $config->setValue($value);
        $this->_em->persist($config);
        $this->_em->flush();
    }

    public function createOrUpdate(string $name, $value): void
    {
        if ($this->has($name)) {
            $config = $this->findOneBy([
                'name' => $name,
            ]);
        } else {
            $config = new Config();
        }
        $config->setName($name);
        $config->setValue($value);
        $this->_em->persist($config);
        $this->_em->flush();
    }
}