<?php

namespace App\Repository\Abstracts;

use App\Entity\Account;
use App\Entity\Budget;
use App\Entity\Planning;
use App\Entity\Transaction;

interface TransactionRepositoryInterface
{

    /**
     * @param Planning $planning
     * @return array|Transaction[]
     */
    public function findByPlanning(Planning $planning): array;

    /**
     * @param Budget $budget
     * @return array|Transaction[]
     */
    public function findByBudget(Budget $budget): array;

    /**
     * @param Budget $budget
     * @param Planning $planning
     * @return array|Transaction[]
     */
    public function findByBudgetAndPlanning(Budget $budget, Planning $planning): array;

    /**
     * @param Budget $budget
     * @param Account $account
     * @return array|Transaction[]
     */
    public function findByBudgetAndAccount(Budget $budget, Account $account): array;
}
