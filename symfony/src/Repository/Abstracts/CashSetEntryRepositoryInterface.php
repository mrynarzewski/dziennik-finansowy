<?php

namespace App\Repository\Abstracts;

use App\Entity\CashSet;
use App\Entity\CashSetEntry;

interface CashSetEntryRepositoryInterface
{
    /**
     * @param CashSet $cashSet
     * @return array|CashSetEntry[]
     */
    public function findByCashSet(CashSet $cashSet): array;

    /**
     * @param CashSet $cashSet
     * @return float
     */
    public function getTotal(CashSet $cashSet): float;

    /**
     * @param CashSet $cashSet
     * @return array<float, integer>
     */
    public function getMapping(CashSet $cashSet): array;
}