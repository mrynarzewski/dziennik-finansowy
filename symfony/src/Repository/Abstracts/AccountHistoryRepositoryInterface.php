<?php


namespace App\Repository\Abstracts;

use App\Entity\Account;
use App\Entity\AccountHistory;
use App\Entity\Budget;

interface AccountHistoryRepositoryInterface
{
    /**
     * find history items by account
     * @param Account $account
     * @return array|AccountHistory[]
     */
    public function findByAccount(Account $account): array;

    /**
     * find history items by budget
     * @param Budget $budget
     * @return array|AccountHistory[]
     */
    public function findByBudget(Budget $budget): array;
}