<?php

namespace App\Repository\Abstracts;

use App\Entity\Budget;
use App\Entity\Manager;

interface BudgetRepositoryInterface
{
    /**
     * @param Manager $manager
     * @return array|Budget[]
     */
    public function findByManager(Manager $manager): array;

    /**
     * @param Manager $manager
     * @param \DateTime|null $startDate
     * @param \DateTime|null $endDate
     * @return array|Budget[]
     */
    public function findByDate(Manager $manager, ?\DateTime $startDate = null, ?\DateTime $endDate = null): array;
}