<?php

namespace App\Repository\Abstracts;

use App\Entity\Manager;
use App\Entity\ManagerRole;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

interface ManagerRoleRepositoryInterface
{

    /**
     * @param Manager $manager
     * @return array|ManagerRole[]
     */
    public function findByManager(Manager $manager): array;

    /**
     * @param DictionaryItem $role
     * @return array|ManagerRole[]
     */
    public function findByRole(DictionaryItem $role): array;
}