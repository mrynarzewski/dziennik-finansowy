<?php

namespace App\Repository\Abstracts;

use App\Entity\Budget;
use App\Entity\Planning;

interface PlanningRepositoryInterface
{
    /**
     * @param Budget $budget
     * @return array|Planning[]
     */
    public function findByBudget(Budget $budget): array;
}