<?php

namespace App\Repository\Abstracts;

use App\Entity\Manager;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Exceptions\InvalidDictionaryItemByDictionaryException;

interface ManagerRepositoryInterface
{
    /**
     * @param string $username
     * @return Manager|null
     */
    public function findByUsername(string $username): ?Manager;

    /**
     * @param array|DictionaryItem[] $roles
     * @return array|Manager[]
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function findByAllRoles(array $roles): array;

    /**
     * @param array|DictionaryItem[] $roles
     * @return array|Manager[]
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function findByAtLeastRoles(array $roles): array;
}