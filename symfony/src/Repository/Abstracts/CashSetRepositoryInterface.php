<?php

namespace App\Repository\Abstracts;

use App\Entity\Account;
use App\Entity\CashSet;

interface CashSetRepositoryInterface
{

    /**
     * @param Account $account
     * @return CashSet|null
     */
    public function findByAccount(Account $account): ?CashSet;
}