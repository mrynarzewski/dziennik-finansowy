<?php

namespace App\Repository\Abstracts;

use App\Entity\Account;
use App\Entity\Manager;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

interface AccountRepositoryInterface
{
    /**
     * find all accounts belongs to manager
     * @param Manager $manager
     * @return array|Account[]
     */
    public function findByManager(Manager $manager): array;

    /**
     * find all accounts belongs to manager and kind
     * @param Manager $manager
     * @param DictionaryItem $kind
     * @return array|Account[]
     */
    public function findByKind(Manager $manager, DictionaryItem $kind): array;

    /**
     * find account belongs to manager and name
     * @param Manager $manager
     * @param string $name
     * @return Account|null
     */
    public function findOneByName(Manager $manager, string $name): ?Account;
}