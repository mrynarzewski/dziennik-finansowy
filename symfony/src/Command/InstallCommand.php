<?php

namespace App\Command;

use App\Enums\AccountKind;
use App\Enums\BudgetDuration;
use App\Enums\Dictionaries;
use App\Enums\ManagerRole;
use App\Service\Traits\EntityManagerAwareTrait;
use App\Service\Traits\ManagerManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryItemServiceAwareTrait;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryServiceAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends Command
{
    protected static $defaultName = 'app:install';

    use ManagerManagerAwareTrait;
    use EntityManagerAwareTrait;
    use DictionaryServiceAwareTrait;
    use DictionaryItemServiceAwareTrait;

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dictionariesRefs = [];
        foreach (Dictionaries::getEnumerators() as $dictionary) {
            $obj = $this->dictionaryService->create($dictionary->getValue(), $dictionary->getName());
            //$dictionariesRefs[$dictionary->getName()] = $obj;
        }
        $dictionaries = [
            AccountKind::getDictionary() => AccountKind::getEnumerators(),
            BudgetDuration::getDictionary() => BudgetDuration::getEnumerators(),
            ManagerRole::getDictionary() => ManagerRole::getEnumerators(),
        ];
        foreach ($dictionaries as $name => $items) {
           $dictionary = $this->entityManager->getRepository(Dictionary::class)
               ->find($name);
           foreach ($items as $item) {
               $this->dictionaryItemService->create($dictionary, $item->getName());
           }
        }
        return Command::SUCCESS;
    }
}
