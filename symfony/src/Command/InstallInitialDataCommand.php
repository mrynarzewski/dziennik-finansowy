<?php

namespace App\Command;

use App\Enums\AccountKind;
use App\Enums\BudgetDuration;
use App\Enums\CashEntryFaceValue;
use App\Enums\Dictionaries;
use App\Enums\ManagerRole;
use App\Enums\PlanningDepartment;
use App\Enums\PlanningKind;
use App\Service\Traits\EntityManagerAwareTrait;
use App\Service\Traits\ManagerManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryItemServiceAwareTrait;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryServiceAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InstallInitialDataCommand extends Command
{
    protected static $defaultName = 'app:install-data';
    protected static $defaultDescription = 'Install data';

    use DictionaryServiceAwareTrait;
    use DictionaryItemServiceAwareTrait;
    use EntityManagerAwareTrait;

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        foreach (Dictionaries::getEnumerators() as $dictionary) {
            $id = $dictionary->getValue();
            $description = $id;
            $extra = null;
            $this->dictionaryService->create($id, $description, $extra);
        }
        $dictionaryItemClasses = [
            AccountKind::getDictionary() => AccountKind::getEnumerators(),
            BudgetDuration::getDictionary() => BudgetDuration::getEnumerators(),
            ManagerRole::getDictionary() => ManagerRole::getEnumerators(),
            CashEntryFaceValue::getDictionary() => CashEntryFaceValue::getEnumerators(),
            PlanningKind::getDictionary() => PlanningKind::getEnumerators(),
            PlanningDepartment::getDictionary() => PlanningDepartment::getEnumerators(),
        ];
        $dictionaryRepository = $this->entityManager->getRepository(Dictionary::class);
        foreach ($dictionaryItemClasses as $dictionaryName => $itemEnumerators) {
            $dictionaryObject = $dictionaryRepository->find($dictionaryName);
            foreach ($itemEnumerators as $itemEnumerator) {
                [$item, $description, $extra] = get_class($itemEnumerator)::process($itemEnumerator);
                $this->dictionaryItemService->create($dictionaryObject, $item, $description, $extra);
            }
        }

        return Command::SUCCESS;
    }
}