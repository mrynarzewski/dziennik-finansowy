<?php

namespace App\Command;

use App\Enums\Dictionaries;
use App\Enums\ManagerRole;
use App\Service\Traits\EntityManagerAwareTrait;
use App\Service\Traits\ManagerManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateAdminCommand extends Command
{
    protected static $defaultName = 'app:create-admin';
    protected static $defaultDescription = 'Create admin manager';

    use ManagerManagerAwareTrait;
    use EntityManagerAwareTrait;

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $username = $io->ask('Username?');
        $password = $io->ask('Password?');
        $repo = $this->entityManager->getRepository(DictionaryItem::class);
        $role = $repo->findOneBy([
            'dictionary' => Dictionaries::MANAGER_ROLE,
            'item' => ManagerRole::ROLE_ADMIN,
        ]);
        $this->managerManager->create($username, $password, [$role]);
        $io->success('Admin created successfully');

        return Command::SUCCESS;
    }
}
