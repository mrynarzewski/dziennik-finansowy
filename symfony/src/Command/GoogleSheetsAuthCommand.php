<?php

namespace App\Command;

use App\Service\External\CommandHandler;
use App\Service\External\CommandParameter;
use App\Service\Traits\GoogleSheetAuthAwareTrait;
use Google\Exception;
use Google_Service_Sheets;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GoogleSheetsAuthCommand extends Command
{
    protected static $defaultName = 'app:google:sheets:auth';
    protected static $defaultDescription = 'Create Google Authentication';

    use GoogleSheetAuthAwareTrait;

    /**
     * @var string
     */
    private $credentials;

    public function __construct(string $credentials)
    {
        parent::__construct();
        $this->credentials = $credentials;
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->authorize(
                $this->credentials,
                new CommandHandler(),
                [Google_Service_Sheets::SPREADSHEETS_READONLY],
                new CommandParameter(
                    $input,
                    $output,
                    $this
                )
            );
        } catch (Exception $e) {
        }
        return Command::SUCCESS;
    }
}

