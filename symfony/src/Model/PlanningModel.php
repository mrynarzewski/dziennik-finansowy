<?php

namespace App\Model;

class PlanningModel
{
    public $kind;
    public $department;
    public $planned_zloty;
    public $planned_gross;
    public $prepaid;
}