<?php


namespace App\Voter;


use App\Enums\ManagerRole;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class DictionaryItemVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, $subject): bool
    {
        if (!$subject instanceof DictionaryItem) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            // the user must be logged in; if not, deny access
            return false;
        }

        /** @var DictionaryItem $item */
        $item = $subject;
        $roles = $user->getRoles();
        $condition = in_array(ManagerRole::ROLE_ADMIN, $roles);
        return $condition;
    }
}