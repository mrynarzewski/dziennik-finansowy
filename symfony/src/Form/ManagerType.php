<?php

namespace App\Form;

use App\Entity\Manager;
use App\Enums\Dictionaries;
use App\Service\Traits\RouterAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ManagerType extends AbstractType
{
    use RouterAwareTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password')
            ->add('roles', DynamicDictionaryItemType::class, [
                'dictionary' => Dictionaries::MANAGER_ROLE,
                'ajax_source_route' => $this->router->generate('app_dictionaryItem_list', [
                    'dictionary' => Dictionaries::MANAGER_ROLE,
                ]),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Manager::class,
        ]);
    }
}
