<?php

namespace App\Form;

use App\Service\Traits\RouterAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryItemRepositoryAwareTrait;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryRepositoryAwareTrait;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DynamicDictionaryItemType extends DynamicSelectType
{
    use DictionaryRepositoryAwareTrait;
    use DictionaryItemRepositoryAwareTrait;
    use RouterAwareTrait;

    /**
     * @var Dictionary
     */
    private $dictionary;

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'dictionary' => null,
            'allow_add' => true,
            'choice_value' => function ($item) {
                if (!$item instanceof DictionaryItem) {
                    return;
                }
                return $item->getId();
            },
            'choice_label' => 'description',
        ]);
        $resolver->setAllowedTypes('dictionary', [
            'string',
            Dictionary::class,
            DictionaryItem::class,
        ]);
        $resolver->setRequired(['dictionary']);
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $dictionary = $options['dictionary'];
        $this->dictionary = $this->getDictionary($dictionary);

        $view->vars['attr']['data-dictionary'] = $this->dictionary->getId();
        $view->vars['attr']['class'] = 'DynamicDictionaryItemType';
    }

    /**
     * @param string|Dictionary|DictionaryItem $dictionary
     * @return Dictionary
     */
    public function getDictionary($dictionary): ?Dictionary
    {
        if (is_null($dictionary)) {
            throw new InvalidArgumentException(sprintf('Dictionary %s not found', $dictionary));
        }
        if ($dictionary instanceof Dictionary) {
            return $dictionary;
        }
        if ($dictionary instanceof DictionaryItem) {
            return $dictionary->getDictionary();
        }
        if (is_string($dictionary)) {
            $dictionaryObj = $this->dictionaryRepository->find($dictionary);
            if (!$dictionaryObj instanceof Dictionary) {
                throw new InvalidArgumentException(sprintf('Dictionary %s not found', $dictionary));
            }
            return $dictionaryObj;
        }

        throw new InvalidArgumentException(sprintf('Dictionary %s not found', $dictionary));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $selector = 'data_collector/passed_options';
        $attributes = $builder->getAttributes()[$selector];
        $dictionary = $options['dictionary'];
        $this->dictionary = $this->getDictionary($dictionary);
        $attributes['constraint'] = [];
        /*$attributes['choices'] = $this->dictionaryItemRepository->findBy([
            'dictionary' => $this->dictionary,
        ]);*/
        $attributes['ajax_source_route'] = $this->router->generate('app_dictionaryItem_list', [
            'dictionary' =>  $this->dictionary->getId(),
        ]);
        $attributes['choice_label'] = 'description';
        $attributes['choice_value'] = function ($item) {
            if (!$item instanceof DictionaryItem) {
                return;
            }
            return $item->getId();
        };
        $builder->setAttribute($selector, $attributes);
    }
}