<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DynamicSelectType extends ChoiceType
{
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        $view->vars['attr']['data-ajax-enable'] = 'true';
        if (!empty($options['ajax_source_route'])) {
            $view->vars['choices'] = [];
            $view->vars['attr']['data-remote-route'] = $options['ajax_source_route'];
        }
        if (!$options['required']) {
            $view->vars['attr']['data-allow-clear'] = 'true';
        }
        if ($options['allow_add']) {
            $view->vars['attr']['data-allow_add'] = 'true';
        }
        $view->vars['attr']['data-extra-params'] = json_encode($options['extra_params']);
        $view->vars['attr']['data-depends-on'] = json_encode($options['depends_on']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'class' => null,
            'choice_value' => 'id',
            'ajax_source_route' => null,
            'allow_add' => false,
            'depends_on' => [],
            'extra_params' => [],
        ]);
        $resolver->setAllowedTypes('allow_add', ['bool']);
    }
}