<?php

namespace App\Form;

use App\Entity\Account;
use App\Enums\Dictionaries;
use App\Service\Traits\RouterAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryItemRepositoryAwareTrait;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryRepositoryAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AccountType extends AbstractType
{
    use RouterAwareTrait;
    use DictionaryItemRepositoryAwareTrait;
    use DictionaryRepositoryAwareTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('reference')
            ->add('kind', Select2EntityType::class, [
                'class' => DictionaryItem::class,
                'remote_route' => 'app_dictionaryItem_listToSelect',
                'remote_params' => [
                    'dictionary' => Dictionaries::ACCOUNT_KIND,
                ],
                'text_property' => 'item',
                'minimum_input_length' => 0,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Account::class,
        ]);
    }
}
