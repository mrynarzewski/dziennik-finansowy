<?php

namespace App\Form;

use App\Enums\Dictionaries;
use App\Service\Traits\EntityManagerAwareTrait;
use App\Service\Traits\RouterAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class PlanningType extends AbstractType
{
    use RouterAwareTrait;
    use EntityManagerAwareTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kind', Select2EntityType::class, [
                'class' => DictionaryItem::class,
                'remote_route' => 'app_dictionaryItem_list',
                'remote_params' => [
                    'dictionary' => Dictionaries::PLANNING_KIND,
                ],
                'text_property' => 'item',
                'minimum_input_length' => 0,
                'width' => 500,
            ])
            ->add('department', Select2EntityType::class, [
                'class' => DictionaryItem::class,
                'remote_route' => 'app_dictionaryItem_list',
                'remote_params' => [
                    'dictionary' => Dictionaries::PLANNING_DEPARTMENT,
                ],
                'text_property' => 'item',
                'allow_add' => [
                    'enabled' => true,
                    'tag_separators' => '[","]',
                ],
                'width' => 500,
            ])
            ->add('planned', MoneyType::class, [
                'currency' => 'PLN',
            ])
            ->add('prepaid', CheckboxType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}