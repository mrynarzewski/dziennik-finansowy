<?php

namespace App\Twig;

use App\Entity\Manager;
use App\Enums\ManagerRole;
use App\Service\Traits\EntityManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryRepositoryAwareTrait;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    use DictionaryRepositoryAwareTrait;
    use EntityManagerAwareTrait;

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            //new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getDictionaries', [$this, 'getDictionaries']),
        ];
    }

    /**
     * @param Manager|UserInterface $manager
     * @return array
     */
    public function getDictionaries(Manager $manager): array
    {
        $dictionaries =  $this->dictionaryRepository->findAll();
        if (in_array(ManagerRole::ROLE_ADMIN, $manager->getRoles())) {
            return $dictionaries;
        }
        $result = [];
        foreach ($dictionaries as $dictionary) {
            if ($dictionary->getExtra()['userCanEdit'] == true) {
                $result[] = $dictionary;
            }
        }
        return $result;
    }
}
