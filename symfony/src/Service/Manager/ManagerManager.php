<?php

namespace App\Service\Manager;

use App\Entity\Manager;
use App\Service\AbstractManager;
use App\Service\Traits\AccountManagerAwareTrait;
use App\Service\Traits\ManagerRoleManagerAwareTrait;
use App\Service\Traits\PasswordEncoderAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

class ManagerManager extends AbstractManager implements ManagerManagerInterface
{

    use PasswordEncoderAwareTrait;
    use ManagerRoleManagerAwareTrait;
    use AccountManagerAwareTrait;

    /**
     * @inheritDoc
     */
    public function create(string $username, string $plainPassword, array $roles = []): Manager
    {
        $this->entityManager->beginTransaction();
        $manager = new Manager();
        $manager->setUsername($username);
        $manager->setPassword($this->passwordEncoder->encodePassword($manager, $plainPassword));
        $this->saveEntity($manager);
        $this->managerRoleManager->setRoles($manager, $roles);
        $this->accountManager->createCashAccount($manager);
        $this->entityManager->commit();

        return $manager;
    }

    /**
     * @inheritDoc
     */
    public function changePassword(Manager $manager, string $oldPassword, string $newPassword): bool
    {
        $manager->setPassword($this->passwordEncoder->encodePassword($manager, $newPassword));
        $this->saveEntity($manager);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function setRoles(Manager $manager, array $roles): void
    {
        $this->managerRoleManager->setRoles($manager, $roles);
    }

    /**
     * @inheritDoc
     */
    public function addRole(Manager $manager, DictionaryItem $role): bool
    {
        $this->managerRoleManager->addRole($manager, $role);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function removeRole(Manager $manager, DictionaryItem $role): bool
    {
        $this->managerRoleManager->removeRole($manager, $role);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function restoreDefaultRoles(Manager $manager): void
    {

    }
}