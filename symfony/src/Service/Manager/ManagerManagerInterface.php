<?php

namespace App\Service\Manager;

use App\Entity\Manager;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

interface ManagerManagerInterface
{
    /**
     * creates an user with given username, plainPassword and roles
     * @param string $username unique username
     * @param string $plainPassword plainPassword
     * @param array|DictionaryItem[] $roles roles
     * @return Manager
     */
    public function create(string $username, string $plainPassword, array $roles = []): Manager;

    /**
     * changes password for given manager
     * @param Manager $manager manager
     * @param string $oldPassword old password to verify ID
     * @param string $newPassword new password to change
     * @return bool if succeeded
     */
    public function changePassword(Manager $manager, string $oldPassword, string $newPassword): bool;

    /**
     * removes all old roles and delete a new set of roles for manager
     * @param Manager $manager manager
     * @param array|DictionaryItem[] $roles new set of roles
     */
    public function setRoles(Manager $manager, array $roles): void;

    /**
     * add new role if not already exists
     * @param Manager $manager manager
     * @param DictionaryItem $role new role to add
     * @return bool if manager has'nt this role then true otherwise false
     */
    public function addRole(Manager $manager, DictionaryItem $role): bool;


    /**
     * remove role if already exists
     * @param Manager $manager manager
     * @param DictionaryItem $role new role to remove
     * @return bool if manager has'nt this role then false otherwise true
     */
    public function removeRole(Manager $manager, DictionaryItem $role): bool;

    /**
     * restore fefault set of roles/role to manager
     * @param Manager $manager manager
     */
    public function restoreDefaultRoles(Manager $manager): void;
}