<?php

namespace App\Service\Account;

use App\Entity\Account;
use App\Entity\Manager;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Symfony\Component\Security\Core\User\UserInterface;

interface AccountManagerInterface
{
    /**
     * create new account for manager with given kind, name and reference
     * @param Manager $manager manager
     * @param DictionaryItem $kind kind of account from Dictionaries::ACCOUNT_KIND
     * @param string $name name preferred unique for you
     * @param string|null $reference short name
     * @return Account
     */
    public function create(Manager $manager, DictionaryItem $kind, string $name, ?string $reference = null): Account;

    /**
     * changes kind of account
     * @param Account $account account
     * @param DictionaryItem $kind new kind of account from Dictionaries::ACCOUNT_KIND
     */
    public function changeKind(Account $account, DictionaryItem $kind): void;

    /**
     * changes kind of account
     * @param Account $account account
     * @param string $name new name
     * @param bool $updateReference give true if update reference according to new name
     */
    public function changeName(Account $account, string $name, bool $updateReference = false): void;

    /**
     * changes kind of account
     * @param Account $account account
     * @param string $reference new reference
     */
    public function changeReference(Account $account, string $reference): void;

    /**
     * gets current balance of given account
     * @param Account $account
     * @return float
     */
    public function getCurrentBalance(Account $account): float;

    /**
     * closes account
     * @param Account $account
     */
    public function close(Account $account): void;

    /**
     * import accounts from Google Spreadsheets
     * @param UserInterface|Manager $manager
     * @param string $spreadSheetId
     * @param string $range
     */
    public function import(Manager $manager, string $spreadSheetId, string $range): void;
}