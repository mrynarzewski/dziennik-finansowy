<?php

namespace App\Service\Account;

use App\Entity\Account;
use App\Entity\Manager;
use App\Enums\AccountKind;
use App\Enums\Dictionaries;
use App\Service\AbstractManager;
use App\Service\Traits\GoogleSheetsImportAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use App\Service\Traits\BudgetManagerAwareTrait;
use Cocur\Slugify\Slugify;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Exceptions\InvalidDictionaryItemByDictionaryException;
use Mrynarzewski\DictionaryBundle\Repository\Traits\DictionaryItemRepositoryAwareTrait;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryItemServiceAwareTrait;

class AccountManager extends AbstractManager implements AccountManagerInterface
{

    use BudgetManagerAwareTrait;

    /** @var Slugify */
    private $slugify;

    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    use GoogleSheetsImportAwareTrait;
    use DictionaryItemRepositoryAwareTrait;
    use DictionaryItemServiceAwareTrait;

    /**
     * @param Manager $manager
     * @param DictionaryItem $kind
     * @param string $name
     * @param string|null $reference
     * @return Account
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function create(Manager $manager, DictionaryItem $kind, string $name, ?string $reference = null): Account
    {
        $result = new Account();
        $result->setManager($manager);
        $this->checkDictionary($kind, Dictionaries::ACCOUNT_KIND);
        $result->setKind($kind);
        $result->setName($name);
        if (!empty($reference)) {
            $result->setReference($this->slugify->slugify($reference));
        } else {
            $result->setReference($this->slugify->slugify($name));
        }
        $this->saveEntity($result);

        return $result;
    }

    /**
     * @param Account $account
     * @param DictionaryItem $kind
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function changeKind(Account $account, DictionaryItem $kind): void
    {
        $this->checkDictionary($kind, Dictionaries::ACCOUNT_KIND);
        $account->setKind($kind);
        $this->saveEntity($account);
    }

    /**
     * @param Account $account
     * @param string $name
     * @param bool $updateReference
     */
    public function changeName(Account $account, string $name, bool $updateReference = false): void
    {
        $account->setName($name);
        if ($updateReference) {
            $account->setReference($this->slugify->slugify($name));
        }
        $this->saveEntity($account);
    }

    /**
     * @param Account $account
     * @param string $reference
     */
    public function changeReference(Account $account, string $reference): void
    {
        $account->setReference($this->slugify->slugify($reference));
        $this->saveEntity($account);
    }

    public function createCashAccount(Manager $manager): ?Account
    {
        if (!is_null($manager->getCashAccount())) {
            return null;
        }
        $accountKindRepository = $this->entityManager->getRepository(DictionaryItem::class);
        $accountKindCash = $accountKindRepository->findOneBy([
            'dictionary' => Dictionaries::ACCOUNT_KIND,
            'item' => AccountKind::CASH,
        ]);
        $account = $this->create($manager, $accountKindCash, AccountKind::CASH);
        $manager->setCashAccount($account);
        $this->saveEntity($manager);

        return $account;
    }

    /**
     * @inheritDoc
     */
    public function getCurrentBalance(Account $account): float
    {
        return 0;
    }

    public function close(Account $account): void
    {
      //  $account->setClosedAt(new \DateTime());
        $this->saveEntity($account);
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function import(Manager $manager, string $spreadSheetId, string $range): void
    {
        $rows = $this->googleSheetsImporter->readCells($spreadSheetId, $range);
        $accountKindDictionary = $this->entityManager->getRepository(Dictionary::class)->find(Dictionaries::ACCOUNT_KIND);
        foreach ($rows as $row) {
            if (empty($row[0])) {
                $kind = $this->dictionaryItemRepository->findOneBy([
                    'dictionary' => $accountKindDictionary,
                    'item' => AccountKind::OTHER,
                ]);
            } else {
                $kind = $this->dictionaryItemRepository->findOneBy([
                    'dictionary' => $accountKindDictionary,
                    'item' => $row[0],
                ]);
                if (empty($kind)) {
                    $kind = $this->dictionaryItemService->create($accountKindDictionary, $row[0]);
                }
            }
            if (empty($row[1])) {
                $row[1] = $row[0];
            }
            $this->create($manager, $kind, $row[1], $row[2]);
        }
    }
}