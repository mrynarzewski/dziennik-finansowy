<?php


namespace App\Service\External;

use App\Service\Abstracts\AuthorizeHandler;
use Exception;
use Google_Client;
use Symfony\Component\Console\Question\Question;

class CommandHandler implements AuthorizeHandler
{

    /**
     * @inheritDoc
     * @throws Exception
     * @param CommandParameter $params
     */
    public function refreshAccessToken(Google_Client $client, $params = null)
    {
        $authUrl = $client->createAuthUrl();
        $input = $params->getInput();
        $output = $params->getOutput();
        $helper = $params->getCommand()->getHelper('question');
        $output->writeln(sprintf("Open the following link in your browser:\n%s\n", $authUrl));
        $question = new Question('Enter verification code: ');

        $authCode = $helper->ask($input, $output, $question);

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        $client->setAccessToken($accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw new Exception(join(', ', $accessToken));
        }
    }
}