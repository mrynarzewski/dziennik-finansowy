<?php

namespace App\Service\External;

use App\Service\Abstracts\AuthorizeHandler;
use Google_Client;
use InvalidArgumentException;

class ErrorHandler implements AuthorizeHandler
{

    public function refreshAccessToken(Google_Client $client, $params = null)
    {
        throw new InvalidArgumentException('');
    }
}