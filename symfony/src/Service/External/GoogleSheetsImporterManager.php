<?php

namespace App\Service\External;

use App\Service\AbstractManager;
use App\Service\Traits\GoogleSheetAuthAwareTrait;
use Google\Exception;
use Google_Client;
use Google_Service_Sheets;

class GoogleSheetsImporterManager extends AbstractManager
{

    use GoogleSheetAuthAwareTrait;

    const GOOGLE_ACCESS_TOKEN = 'googleAccessToken';
    const APP_NAME = 'Google Sheets Finance';
    const ACCESS_TYPE = 'offline';
    const PROMPT = 'select_account consent';

    /** @var string */
    private $credentialPath;

    public function __construct(string $credentialPath)
    {
        $this->credentialPath = $credentialPath;
    }

    /**
     * @param array $scopes
     * @return Google_Client
     * @throws Exception
     */
    public function getClient($scopes = []): Google_Client
    {
        return $this->authorize(
            $this->credentialPath,
            new ErrorHandler(),
            $scopes
        );
    }

    /**
     * @param string $spreadsheetId
     * @param string $range
     * @return array
     * @throws Exception
     */
    public function readCells(string $spreadsheetId, string $range): array
    {
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();

        return $values;
    }

}