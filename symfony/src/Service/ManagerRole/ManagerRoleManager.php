<?php

namespace App\Service\ManagerRole;

use App\Entity\Manager;
use App\Entity\ManagerRole;
use App\Enums\Dictionaries;
use App\Service\AbstractManager;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

class ManagerRoleManager extends AbstractManager implements ManagerRoleManagerInterface
{

    /**
     * @inheritDoc
     */
    public function getRoles(Manager $manager): array
    {
        return $manager->getManagerRoles();
    }

    /**
     * @inheritDoc
     */
    public function getManagers(DictionaryItem $role): array
    {
        $this->checkDictionary($role, Dictionaries::MANAGER_ROLE);
        $repo = $this->entityManager->getRepository(ManagerRole::class);
        $managerRoles = $repo->findByRole($role);
        $result = [];
        foreach ($managerRoles as $managerRole) {
            $result[] = $managerRole->getManager();
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function addRole(Manager $manager, DictionaryItem $role): ManagerRole
    {
        $managerRole = new ManagerRole();
        $managerRole->setManager($manager);
        $managerRole->setRole($role);

        $this->saveEntity($managerRole);

        return $managerRole;
    }

    /**
     * @inheritDoc
     */
    public function setRoles(Manager $manager, array $roles): void
    {
        foreach ($roles as $role) {
            if (!$role instanceof DictionaryItem){ // && $this->checkDictionary($role, Dictionaries::MANAGER_ROLE)) {
                $role = $this->entityManager->getRepository(DictionaryItem::class)->findOneBy([
                    'dictionary' => Dictionaries::MANAGER_ROLE,
                    'item' => $role,
                ]);
            }
            if ($role instanceof DictionaryItem) {
                $this->addRole($manager,$role);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function removeRole(Manager $manager, DictionaryItem $role): bool
    {
       return false;
    }

    /**
     * @inheritDoc
     */
    public function removeRoles(Manager $manager, array $roles): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function restoreDefaultRoles(Manager $manager): void
    {
        // TODO: Implement restoreDefaultRoles() method.
    }
}