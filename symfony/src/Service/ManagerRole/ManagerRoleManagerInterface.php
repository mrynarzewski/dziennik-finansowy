<?php

namespace App\Service\ManagerRole;

use App\Entity\Manager;
use App\Entity\ManagerRole;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

interface ManagerRoleManagerInterface
{
    /**
     * gets all roles for given manager
     * @param Manager $manager manager
     * @return array|DictionaryItem[] roles of manager
     */
    public function getRoles(Manager $manager): array;

    /**
     * gets all manager with given role
     * @param DictionaryItem $role role
     * @return array|Manager[]
     */
    public function getManagers(DictionaryItem $role): array;

    /**
     * add role to manager
     * @param Manager $manager manager
     * @param DictionaryItem $role role
     * @return ManagerRole result
     */
    public function addRole(Manager $manager, DictionaryItem $role): ManagerRole;

    /**
     * removes all old roles and delete a new set of roles for manager
     * @param Manager $manager manager
     * @param array|DictionaryItem[] $roles new set of roles
     */
    public function setRoles(Manager $manager, array $roles): void;

    /**
     * remove role if already exists
     * @param Manager $manager manager
     * @param DictionaryItem $role role to remove
     * @return bool if manager has'nt this role then false otherwise true
     */
    public function removeRole(Manager $manager, DictionaryItem $role): bool;

    /**
     * remove role if already exists
     * @param Manager $manager manager
     * @param array|DictionaryItem[] $roles roles to remove
     * @return bool if manager has'nt this role then false otherwise true
     */
    public function removeRoles(Manager $manager, array $roles): bool;

    /**
     * restore fefault set of roles/role to manager
     * @param Manager $manager manager
     */
    public function restoreDefaultRoles(Manager $manager): void;
}