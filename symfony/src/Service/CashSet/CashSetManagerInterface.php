<?php

namespace App\Service\CashSet;

use App\Entity\Account;
use App\Entity\Budget;
use App\Entity\CashSet;

interface CashSetManagerInterface
{
    /**
     * @param Account $account
     * @param Budget $budget
     * @return CashSet
     */
    public function create(Account $account, Budget $budget): CashSet;

    /**
     * @param CashSet $cashSet
     * @return float
     */
    public function sum(CashSet $cashSet): float;
}