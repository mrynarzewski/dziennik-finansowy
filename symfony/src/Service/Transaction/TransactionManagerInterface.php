<?php

namespace App\Service\Transaction;

use App\Entity\Account;
use App\Entity\Budget;
use App\Entity\Planning;
use App\Entity\Transaction;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

interface TransactionManagerInterface
{
    /**
     * registers income transaction
     * @param Budget $budget current budget
     * @param Planning $planning planning of transaction
     * @param DictionaryItem $place place
     * @param string $description description
     * @param $quote quote
     * @param Account $account account charged or downloaded
     * @param \DateTime|null $transDate transaction date (default: now)
     * @return Transaction
     */
    public function registerIncome(
        Budget $budget,
        Planning $planning,
        DictionaryItem $place,
        string $description,
        $quote,
        Account $account,
        \DateTime $transDate = null
    ): Transaction;

    /**
     * registers outcome transaction
     * @param Budget $budget current budget
     * @param Planning $planning planning of transaction
     * @param DictionaryItem $place place
     * @param string $description description
     * @param $quote quote
     * @param Account $account account charged or downloaded
     * @param \DateTime|null $transDate transaction date (default: now)
     * @return Transaction
     */
    public function registerOutcome(
        Budget $budget,
        Planning $planning,
        DictionaryItem $place,
        string $description,
        $quote,
        Account $account,
        \DateTime $transDate = null
    ): Transaction;

    /**
     * marks transaction as accounted
     * @param Transaction $transaction transaction
     * @param \DateTime|null $accountDate accountDate or NOW
     * @return \DateTime|null accountDate result
     */
    public function account(Transaction $transaction, \DateTime $accountDate = null): ?\DateTime;
}