<?php

namespace App\Service;

use App\Entity\AbstractEntity;
use App\Service\Traits\EntityManagerAwareTrait;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Exceptions\InvalidDictionaryItemByDictionaryException;

abstract class AbstractManager
{
    use EntityManagerAwareTrait;

    /**
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    protected function saveEntity($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    /**
     * @param AbstractEntity $entity
     * @return AbstractEntity
     */
    protected function removeEntity($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return $entity;
    }

    /**
     * @param DictionaryItem $item
     * @param string $dictionary
     * @throws InvalidDictionaryItemByDictionaryException
     */
    protected function checkDictionary(DictionaryItem $item, string $dictionary)
    {
        if ($item->getDictionary()->getId() != $dictionary) {
            $expectedDictionary = $this->entityManager->getRepository(Dictionary::class)->find($dictionary);
            throw new InvalidDictionaryItemByDictionaryException($item->getDictionary(), $expectedDictionary);
        }
    }
}