<?php

namespace App\Service;

use ArrayAccess;
use ArrayIterator;
use Countable;
use DomainException;
use IteratorAggregate;

class DistinctArray implements IteratorAggregate, Countable, ArrayAccess
{
    protected $store = array();

    public function __construct(array $initialValues)
    {
        foreach ($initialValues as $key => $value) {
            $this->offsetSet($key, $value);
        }
    }

    final public function offsetSet($offset, $value)
    {
        if (in_array($value, $this->store, true)) {
            throw new DomainException('Values must be unique!');
        }

        if (null === $offset) {
            array_push($this->store, $value);
        } else {
            $this->store[$offset] = $value;
        }
    }

    final public function offsetGet($offset)
    {
        return $this->store[$offset];
    }

    final public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->store);
    }

    final public function offsetUnset($offset)
    {
        unset($this->store[$offset]);
    }

    final public function count(): int
    {
        return count($this->store);
    }

    final public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->store);
    }

    public function isSubset(DistinctArray $param): bool
    {
        foreach ($this->store as $value) {
            if (!in_array($value, $param->store)) {
                return false;
            }
        }
        return true;
    }
}