<?php


namespace App\Service\Planning;


use App\Entity\Budget;
use App\Entity\Planning;
use App\Enums\Dictionaries;
use App\Service\AbstractManager;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Service\Traits\DictionaryItemServiceAwareTrait;

class PlanningManager extends AbstractManager implements PlanningManagerInterface
{
    use DictionaryItemServiceAwareTrait;

    public function create(Budget $budget, DictionaryItem $kind, DictionaryItem $department, float $planned, bool $prepaid = false): Planning
    {
        $planning = new Planning();
        $planning->setBudget($budget);
        $planning->setKind($kind);
        if ($department->getId() == null) {
            $planningDepartment = $this->entityManager->getRepository(Dictionary::class)->find(Dictionaries::PLANNING_DEPARTMENT);
            $department = $this->dictionaryItemService->create(
                $planningDepartment,
                $department->getDescription(),
                $department->getDescription()
            );
        }
        $planning->setDepartment($department);
        $planning->setPlanned($planned);
        $planning->setPrepaid($prepaid);
        $this->saveEntity($planning);

        return $planning;
    }

    public function createIncome(Budget $budget, DictionaryItem $department, float $planned, bool $prepaid = false): Planning
    {
        $planning = new Planning();
        return $planning;
    }

    public function createOutcome(Budget $budget, DictionaryItem $department, float $planned, bool $prepaid = false): Planning
    {
        $planning = new Planning();
        return $planning;
    }

    public function createBoth(Budget $budget, DictionaryItem $department, float $planned, bool $prepaid = false): Planning
    {
        $planning = new Planning();
        return $planning;
    }

    public function updatePlanned(Planning $planning, float $planned): float
    {
        return $planning->getPlanned();
    }

    public function calculateRemained(Planning $planning): float
    {
        return 0;
    }

    public function calculateUsed(Planning $planning): float
    {
        return 0;
    }

    public function divideByPercentage(Planning $planning, float $percentage, DictionaryItem $otherDepartment): Planning
    {
        $planning = new Planning();
        return $planning;
    }
}