<?php

namespace App\Service\Planning;

use App\Entity\Budget;
use App\Entity\Planning;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Exceptions\InvalidDictionaryItemByDictionaryException;

interface PlanningManagerInterface
{
    /**
     * create planing for given budget with given kind, department and planned
     * @param Budget $budget budget
     * @param DictionaryItem $kind kind (income, outcome, mixed) of planning
     * @param DictionaryItem $department description of planned area
     * @param float $planned count of money to spend in this planning
     * @param bool $prepaid
     * @return Planning
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function create(Budget $budget, DictionaryItem $kind, DictionaryItem $department, float $planned, bool $prepaid = false): Planning;

    /**
     * create planned income with given budget, department, planned money and prepaid
     * @param Budget $budget
     * @param DictionaryItem $department
     * @param float $planned
     * @param bool $prepaid
     * @return Planning
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function createIncome(Budget $budget, DictionaryItem $department, float $planned, bool $prepaid = false): Planning;

    /**
     * create planned outcoming with given budget, department, planned money and prepaid
     * @param Budget $budget
     * @param DictionaryItem $department
     * @param float $planned
     * @param bool $prepaid
     * @return Planning
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function createOutcome(Budget $budget, DictionaryItem $department, float $planned, bool $prepaid = false): Planning;

    /**
     * create planned outcoming or income with given budget, department, planned money and prepaid
     * @param Budget $budget
     * @param DictionaryItem $department
     * @param float $planned
     * @param bool $prepaid
     * @return Planning
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function createBoth(Budget $budget, DictionaryItem $department, float $planned, bool $prepaid = false): Planning;

    /**
     * @param Planning $planning
     * @param float $planned
     * @return float
     */
    public function updatePlanned(Planning $planning, float $planned): float;

    /**
     * @param Planning $planning
     * @return float
     */
    public function calculateRemained(Planning $planning): float;

    /**
     * @param Planning $planning
     * @return float
     */
    public function calculateUsed(Planning $planning): float;

    /**
     * divide existing Planning between two less divided percentage planned money
     * @param Planning $planning
     * @param float $percentage
     * @param DictionaryItem $otherDepartment
     * @return Planning
     * @throws InvalidDictionaryItemByDictionaryException
     */
    public function divideByPercentage(Planning $planning, float $percentage, DictionaryItem $otherDepartment): Planning;
}