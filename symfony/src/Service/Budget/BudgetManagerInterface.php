<?php

namespace App\Service\Budget;

use App\Entity\Budget;
use App\Entity\Manager;
use App\Enums\BudgetDuration;
use DateTime;

interface BudgetManagerInterface
{
    /**
     * @param Manager $manager
     * @param DateTime $startDate
     * @param BudgetDuration $duration
     * @return Budget
     */
    public function create(Manager $manager, DateTime $startDate, BudgetDuration $duration): Budget;

    /**
     * @param int $year
     * @param int $length
     * @return array|string[] of date
     */
    public function getTransMonths(int $year, int $length = 12): array;

    /**
     * get current budget for given manager
     * @param Manager $manager
     * @return Budget|null
     */
    public function getCurrentBudget(Manager $manager): ?Budget;
}