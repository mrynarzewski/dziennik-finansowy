<?php

namespace App\Service\Budget;

use App\Entity\Budget;
use App\Entity\Manager;
use App\Enums\BudgetDuration;
use App\Service\AbstractManager;
use DateTime;

class BudgetManager extends AbstractManager implements BudgetManagerInterface
{

    /**
     * @inheritdoc
     */
    public function create(Manager $manager, DateTime $startDate, BudgetDuration $durationEnum): Budget
    {
        $result = new Budget();
        $result->setManager($manager);
        $result->setStartDate($startDate);
        $startDateClone = clone $startDate;
        $duration = \DateInterval::createFromDateString($durationEnum->getValue());
        $endDate = $startDateClone->add($duration);
        $result->setEndDate($endDate);
        $this->saveEntity($result);

        return $result;
    }

    public function getTransMonths(int $year, int $length = 12): array
    {
        $months = [];
        for ($month = 1; $month <= 12; $month += 1) {
            $months[] = new DateTime("$year-$month-01");
        }

        return $months;
    }

    /**
     * @inheritDoc
     */
    public function getCurrentBudget(Manager $manager): ?Budget
    {
        return $this->entityManager->getRepository(Budget::class)
            ->createQueryBuilder('b')
            ->where(':now BETWEEN b.startDate AND b.endDate')
            ->andWhere('b.manager = :manager')
            ->setParameter('now', new DateTime())
            ->setParameter('manager', $manager)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}