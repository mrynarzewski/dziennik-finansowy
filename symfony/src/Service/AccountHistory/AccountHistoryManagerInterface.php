<?php

namespace App\Service\AccountHistory;

use App\Entity\Account;
use App\Entity\AccountHistory;
use App\Entity\Budget;

interface AccountHistoryManagerInterface
{
    /**
     * archive account and create account historyItem
     * @param Account $account account to archive
     * @param Budget $budget current budget
     * @return AccountHistory archived account
     */
    public function archive(Account $account, Budget $budget): AccountHistory;
}