<?php
/**
 * Created by PhpStorm.
 * User: marek
 * Date: 25.01.21
 * Time: 21:57
 */

namespace App\Service\CashSetEntry;


use App\Entity\CashSet;
use App\Entity\CashSetEntry;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;

interface CashSetEntryManagerInterface
{

    /**
     * @param CashSet $cashSet
     * @param DictionaryItem $faceValue
     * @param int $value
     * @return CashSetEntry
     */
    public function createSingle(CashSet $cashSet, DictionaryItem $faceValue, int $value): CashSetEntry;

    /**
     * @param CashSetEntry $entry
     * @param int $value
     * @return bool
     */
    public function updateValue(CashSetEntry $entry, int $value): bool;

}