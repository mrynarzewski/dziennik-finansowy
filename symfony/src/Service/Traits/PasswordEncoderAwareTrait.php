<?php

namespace App\Service\Traits;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

trait PasswordEncoderAwareTrait
{

    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /**
     * @required
     */
    public function setPasswordEncoder(UserPasswordEncoderInterface $passwordEncoder): void
    {
        $this->passwordEncoder = $passwordEncoder;
    }


}