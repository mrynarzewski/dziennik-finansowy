<?php

namespace App\Service\Traits;

use App\Service\Manager\ManagerManagerInterface;

trait ManagerManagerAwareTrait
{
    /**
     * @var ManagerManagerInterface
     */
    private $managerManager;

    /**
     * @required
     */
    public function setManagerManager(ManagerManagerInterface $managerManager): void
    {
        $this->managerManager = $managerManager;
    }


}