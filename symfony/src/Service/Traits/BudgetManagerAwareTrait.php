<?php

namespace App\Service\Traits;

use App\Service\Budget\BudgetManagerInterface;

trait BudgetManagerAwareTrait
{
    /** @var BudgetManagerInterface */
    private $budgetManager;

    /**
     * @required
     * @param BudgetManagerInterface $budgetManager
     */
    public function setBudgetManager(BudgetManagerInterface $budgetManager): void
    {
        $this->budgetManager = $budgetManager;
    }
}