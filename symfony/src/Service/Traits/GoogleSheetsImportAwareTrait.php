<?php


namespace App\Service\Traits;


use App\Service\External\GoogleSheetsImporterManager;

trait GoogleSheetsImportAwareTrait
{
    /** @var GoogleSheetsImporterManager */
    private $googleSheetsImporter;

    /**
     * @required
     * @param GoogleSheetsImporterManager $googleSheetsImporter
     */
    public function setGoogleSheetsImporter(GoogleSheetsImporterManager $googleSheetsImporter): void
    {
        $this->googleSheetsImporter = $googleSheetsImporter;
    }
}