<?php

namespace App\Service\Traits;

use App\Service\ManagerRole\ManagerRoleManagerInterface;

trait ManagerRoleManagerAwareTrait
{
    /**
     * @var ManagerRoleManagerInterface
     */
    private $managerRoleManager;

    /**
     * @param ManagerRoleManagerInterface $managerRoleManager
     * @required
     */
    public function setManagerRoleManager(ManagerRoleManagerInterface $managerRoleManager): void
    {
        $this->managerRoleManager = $managerRoleManager;
    }


}