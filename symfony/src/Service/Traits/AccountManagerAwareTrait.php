<?php

namespace App\Service\Traits;

use App\Service\Account\AccountManagerInterface;

trait AccountManagerAwareTrait
{
    /** @var AccountManagerInterface */
    private $accountManager;

    /**
     * @required
     * @param AccountManagerInterface $accountManager
     */
    public function setAccountManager(AccountManagerInterface $accountManager): void
    {
        $this->accountManager = $accountManager;
    }
}