<?php


namespace App\Service\Traits;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

trait SessionAwareTrait
{
    /** @var SessionInterface */
    private $session;

    /**
     * @required
     * @param SessionInterface $session
     */
    public function setSession(SessionInterface $session): void
    {
        $this->session = $session;
    }
}