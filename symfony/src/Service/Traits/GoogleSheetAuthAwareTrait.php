<?php


namespace App\Service\Traits;

use App\Entity\Config;
use App\Service\Abstracts\AuthorizeHandler;
use App\Service\External\GoogleSheetsImporterManager;
use Google\Exception;
use Google_Client;
use Google_Service_Sheets;

trait GoogleSheetAuthAwareTrait
{
    use EntityManagerAwareTrait;

    /**
     * @param string $credentials
     * @param AuthorizeHandler $handler
     * @param string[] $scopes
     * @param mixed|null $params
     * @return Google_Client
     * @throws Exception
     */
    public function authorize(string $credentials, AuthorizeHandler $handler, array $scopes = [], $params = null): Google_Client
    {
        $client = new Google_Client();
        $client->setApplicationName(GoogleSheetsImporterManager::APP_NAME);
        if (empty($scopes)) {
            $scopes = [Google_Service_Sheets::SPREADSHEETS_READONLY];
        }
        $client->setScopes($scopes);
        $client->setAuthConfig($credentials);
        $client->setAccessType(GoogleSheetsImporterManager::ACCESS_TYPE);
        $client->setPrompt(GoogleSheetsImporterManager::PROMPT);

        $repository = $this->entityManager->getRepository(Config::class);
        if ($repository->has(GoogleSheetsImporterManager::GOOGLE_ACCESS_TOKEN)) {
            $accessToken = $repository->get(GoogleSheetsImporterManager::GOOGLE_ACCESS_TOKEN);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                $handler->refreshAccessToken($client, $params);
            }
            $repository->createOrUpdate(
                GoogleSheetsImporterManager::GOOGLE_ACCESS_TOKEN,
                $client->getAccessToken()
            );
        }
        return $client;
    }
}