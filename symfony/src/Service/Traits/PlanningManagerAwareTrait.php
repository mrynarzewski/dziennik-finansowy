<?php


namespace App\Service\Traits;


use App\Service\Planning\PlanningManagerInterface;

trait PlanningManagerAwareTrait
{
    /** @var PlanningManagerInterface */
    private $planningManager;

    /**
     * @required
     * @param PlanningManagerInterface $planningManager
     */
    public function setPlanningManager(PlanningManagerInterface $planningManager): void
    {
        $this->planningManager = $planningManager;
    }
}