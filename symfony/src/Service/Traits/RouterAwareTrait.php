<?php

namespace App\Service\Traits;

use Symfony\Component\Routing\RouterInterface;

trait RouterAwareTrait
{
    /** @var RouterInterface */
    private $router;

    /**
     * @required
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router): void
    {
        $this->router = $router;
    }


}