<?php

namespace App\Service\Summary;

use App\Entity\Account;
use App\Entity\Budget;
use App\Entity\Summary;

interface SummaryManagerInterface
{

    /**
     * create summary for given account and budget
     * @param Account $account
     * @param Budget $budget
     * @return Summary
     */
    public function summary(Account $account, Budget $budget): Summary;
}