<?php


namespace App\Service\Abstracts;

interface AuthorizeHandler
{

    /**
     * @param \Google_Client $client
     */
    public function refreshAccessToken(\Google_Client $client, array $params = []);
}