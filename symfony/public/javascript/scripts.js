function select2_init() {
    $('select[data-ajax-enable="true"]').each(function () {
        const self = $(this);
        const options = {};
        if (undefined !== self.attr('data-placeholder') ) {
            options.placeholder = self.data('placeholder');
            if ('true' == self.attr('data-allow-clear') ) {
                options.allowClear = true;
            }
        }
        if ('true' == self.attr('data-allow_add') ) {
            options.tags = true;
        }
        if (undefined !== self.attr('data-remote-route') ) {
            options.ajax = {
                url: self.data('remote-route'),
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    const result = {
                        'term': params.term,
                        'q': params.q,
                        '_type': params._type,
                        'page': params.page || 1
                    };
                    const extraParams = self.data('extra-params');
                    $.each(extraParams, function (idx, obj) {
                        result[idx] = obj;
                    });
                    const dependsOn = self.data('depends-on');
                    const form = self.closest('form');
                    const formData = form.serializeJSON()[form.attr('name')];
                    $.each(dependsOn, function (idx, obj) {
                        if (formData[obj]) {
                            result[idx] = formData[obj];
                        }
                    });

                    return result;
                },
                cache: true,
                beforeSend: function( jqXHR, settings ) {

                },
                error: function (jqXHR, status, error) {
                    if (jqXHR.status == 401) {
                    }
                }
            }
        }
        self.select2(options);
    });

    $('.DynamicDictionaryItemType').on('select2:select', function (e) {
        const self = $(this);
        if (Number.isInteger(e.params.data.id)) {
            return;
        }
        addDictionaryItem(self.data('dictionary'), e.params.data.id, self);
    });
}
function addDictionaryItem(dictionary, item, field) {
    $.ajax({
        url: '/dictionary-item/' + dictionary,
        method: 'POST',
        data: {
            item: item
        },
        success: function (response) {
            field.find('option[value="' + item + '"]').remove();
            var newOption = new Option(response.text, response.id, true, true);
            field.append(newOption).trigger('change');
        },
    });
}
select2_init();