<?php

namespace App\Tests\Service\Account;

use App\Entity\Manager;
use App\Enums\Dictionaries;
use App\Service\Account\AccountManager;
use App\Service\Budget\BudgetManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Mrynarzewski\DictionaryBundle\Entity\Dictionary;
use Mrynarzewski\DictionaryBundle\Entity\DictionaryItem;
use Mrynarzewski\DictionaryBundle\Exceptions\InvalidDictionaryItemByDictionaryException;
use Mrynarzewski\DictionaryBundle\Repository\DictionaryRepository;
use PHPUnit\Framework\TestCase;

class AccountManagerTest extends TestCase
{
    /** @var AccountManager */
    private $accountManager;

    public function setUp(): void
    {
        parent::setUp();
        $this->accountManager = new AccountManager();
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $this->accountManager->setEntityManager($entityManager);
        $budgetManager = $this->createMock(BudgetManagerInterface::class);
        $this->accountManager->setBudgetManager($budgetManager);
        $dictionaryRepository = $this->createMock(DictionaryRepository::class);
        $dictionaryRepository->expects($this->any())
            ->method('find')
            ->willReturnCallback([$this, 'dictionaryRepository_find']);
        $entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnMap([
                [Dictionary::class, $dictionaryRepository],
            ]);
    }

    public function dictionaryRepository_find($id): ?Dictionary
    {
        $dictionary = $this->createMock(Dictionary::class);
        $dictionary->expects($this->any())
            ->method('getId')
            ->willReturn($id);
        return $dictionary;
    }

    /**
     * @param string $dictionaryId
     * @param string $name
     * @param string|null $reference
     * @param string|null $expectedReference
     * @param string|null $exception
     * @throws InvalidDictionaryItemByDictionaryException
     * @dataProvider createDataProvider
     */
    public function testCreate(
        string $dictionaryId,
        string $name,
        ?string $reference,
        ?string $expectedReference,
        ?string $exception = null
    ) {
        $manager = $this->createMock(Manager::class);
        $kind = $this->createMock(DictionaryItem::class);
        $dictionary = $this->createMock(Dictionary::class);
        $dictionary->expects($this->any())
            ->method('getId')
            ->willReturn($dictionaryId);
        $kind->expects($this->any())
            ->method('getDictionary')
            ->willReturn($dictionary);
        if (!empty($exception)) {
            $this->expectException($exception);
        }
        $account = $this->accountManager->create($manager, $kind, $name, $reference);
        $this->assertEquals($expectedReference, $account->getReference());
    }

    public function createDataProvider(): array
    {
        return [
            [
                Dictionaries::ACCOUNT_KIND,
                'Testowy budżet',
                null,
                'testowy-budzet',
                null,
            ],
            [
                Dictionaries::ACCOUNT_KIND,
                'Testowy budżet',
                'testowy-budzet',
                'testowy-budzet',
                null,
            ],
            [
                Dictionaries::PLANNING_DEPARTMENT,
                'Testowy budżet',
                'testowy-budzet',
                'testowy-budzet',
                InvalidDictionaryItemByDictionaryException::class,
            ],
        ];
    }
}
