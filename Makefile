env:
	cp symfony/.env.example symfony/.env
	cp symfony/.env.test.example symfony/.env.test
	ln -s '$(shell pwd)/symfony/.env' '$(shell pwd)/.env'
	ln -s '$(shell pwd)/symfony/.env.test' '$(shell pwd)/.env.test'

start:
	docker-compose up -d

stop:
	docker-compose down

bash:
	docker-compose run --rm --user "$(shell id -u):$(shell id -g)" php bash

mysql:
	docker exec -it dziennikfinansowy_mysql_1 sh -c 'mysql -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD"'

backup-db:
	docker exec dziennikfinansowy_mysql_1 sh -c 'exec mysqldump -u root -p"$$MYSQL_ROOT_PASSWORD" $$MYSQL_DATABASE' > finance-diary.sql

import-db:
	docker exec -it dziennikfinansowy_mysql_1 sh -c 'mysql -u "$$MYSQL_USER" -p"$$MYSQL_PASSWORD"' < finance-diary.sql
